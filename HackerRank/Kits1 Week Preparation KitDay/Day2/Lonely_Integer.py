from collections import Counter


def lonelyinteger(a):
    return Counter(a).most_common()[-1][0]
