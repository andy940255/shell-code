def diagonalDifference(arr):
    n = len(arr)
    a = 0
    b = 0
    for i in range(n):
        a += arr[i][i]
        b += arr[i][n - 1 - i]
    return abs(a - b)
