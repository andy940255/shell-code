n = int(input())
queue = []
for i in range(n):
    query = input().split()
    if query[0] == "1":
        queue.append(query[1])
    elif query[0] == "2":
        queue.pop(0)
    elif query[0] == "3":
        print(queue[0])
