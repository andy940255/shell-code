def isBalanced(s):
    left = ["[", "{", "("]
    right = ["]", "}", ")"]
    stack = []
    for b in s:
        if b in left:
            stack.append(b)
        elif b in right:
            if not stack or stack.pop() != left[right.index(b)]:
                return "NO"
    if stack:
        return "NO"
    else:
        return "YES"
