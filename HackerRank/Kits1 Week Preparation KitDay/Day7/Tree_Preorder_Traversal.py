"""
Node is defined as
self.left (the left child of the node)
self.right (the right child of the node)
self.info (the value of the node)
"""


def preOrder(root):
    stack = [root]
    while stack:
        node = stack.pop()
        if node:
            print(node.info, end=" ")
            stack.append(node.right)
            stack.append(node.left)
