def noPrefix1(words):
    # Time out
    length = []
    for i in range(len(words)):
        length.append(len(words[i]))
        for j in range(i):
            if words[j] == words[i][: length[j]] or words[i] == words[j][: length[i]]:
                print("BAD SET")
                print(words[i])
                return
    print("GOOD SET")


def noPrefix2(words):
    # Optimal Solution
    preSetA = set()  # Confirm whether word is the prefix of other words
    preSetB = set()  # Confirm whether other words are prefixes of word
    for word in words:
        n = len(word)
        if word in preSetB:
            print("BAD SET")
            print(word)
            return

        for i in range(1, n + 1):
            pre = word[:i]
            if pre in preSetA:
                print("BAD SET")
                print(word)
                return
            preSetB.add(pre)
        preSetA.add(word)
    print("GOOD SET")
