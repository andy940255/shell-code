def decodeHuff(root, s):
    empty = root.data
    cur = root
    for i in s:
        if i == "1":
            cur = cur.right
        else:
            cur = cur.left
        if cur.data != empty:
            print(cur.data, end="")
            cur = root
