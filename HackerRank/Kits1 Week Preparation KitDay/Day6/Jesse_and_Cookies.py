# Complete the 'cookies' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER k
#  2. INTEGER_ARRAY A
#
import bisect


# Slow
def cookies(k, A):
    A.sort()
    step = 0
    while len(A) >= 2 and A[0] < k:
        newNum = A.pop(0) + A.pop(0) * 2
        A.insert(bisect.bisect(A, newNum), newNum)
        step += 1

    if A and min(A) >= k:
        return step
    else:
        return -1


import heapq


# Fast
def cookies1(k, A):
    heapq.heapify(A)
    step = 0
    while len(A) >= 2 and A[0] < k:
        firstNum = heapq.heappop(A)
        secondNum = heapq.heappop(A)
        newNum = firstNum + secondNum * 2
        heapq.heappush(A, newNum)
        step += 1

    if A and A[0] >= k:
        return step
    else:
        return -1
