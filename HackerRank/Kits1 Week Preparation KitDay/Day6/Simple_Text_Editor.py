S = ""
undoArr = []
n = int(input())
for i in range(n):
    query = input().split()
    if query[0] == "1":
        undoArr.append(S)
        S += query[1]
    elif query[0] == "2":
        undoArr.append(S)
        S = S[: len(S) - int(query[1])]
    elif query[0] == "3":
        print(S[int(query[1]) - 1])
    elif query[0] == "4":
        S = undoArr.pop()
