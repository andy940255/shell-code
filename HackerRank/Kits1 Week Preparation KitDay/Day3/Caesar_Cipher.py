# Complete the 'caesarCipher' function below.
#
# The function is expected to return a STRING.
# The function accepts following parameters:
#  1. STRING s
#  2. INTEGER k


def caesarCipher(s, k):
    ans = ""
    for i in s:
        if ord("a") <= ord(i) <= ord("z"):
            ans += chr(ord("a") + (ord(i) - ord("a") + k) % 26)
        elif ord("A") <= ord(i) <= ord("Z"):
            ans += chr(ord("A") + (ord(i) - ord("A") + k) % 26)
        else:
            ans += i
    return ans
