# Complete the 'minimumBribes' function below.
#
# The function accepts INTEGER_ARRAY q as parameter.
#


def minimumBribes(q):
    # Time Out
    ans = 0
    n = len(q)
    arr = [i for i in range(1, n + 1)]
    for idx, ele in enumerate(q):
        f = arr.index(ele)
        if f - idx > 2:
            print("Too chaotic")
            return
        elif 1 <= f - idx <= 2:
            ans += f - idx
            arr[idx : f + 1] = [arr[f]] + arr[idx:f]
    print(ans)


def minimumBribes1(q):
    # Solution from @charl_normandin
    ans = 0
    n = len(q)
    for i in range(n):
        if q[i] - (i + 1) > 2:
            print("Too chaotic")
            return
        for j in range(max(0, q[i] - 2), i):
            if q[j] > q[i]:
                ans += 1
    print(ans)
