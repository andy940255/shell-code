# Complete the 'superDigit' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. STRING n
#  2. INTEGER k


def superDigit(n, k):
    firstSum = 0
    for i in n:
        firstSum += int(i)
    k = str(firstSum * k)

    while len(k) > 1:
        sumNum = 0
        for i in k:
            sumNum += int(i)
        k = str(sumNum)
    return k


def superDigit1(n, k):
    # Good Solution from @trietkd1475369
    sum_n = 0
    for x in n:
        sum_n += int(x)
    mod = ((sum_n % 9) * (k % 9)) % 9
    if mod == 0:
        mod = 9
    return mod
