# Complete the 'gridChallenge' function below.
#
# The function is expected to return a STRING.
# The function accepts STRING_ARRAY grid as parameter.


def gridChallenge(grid):
    x = len(grid)
    y = len(grid[0])
    newG = []
    for i in range(x):
        newG.append(sorted(list(grid[i])))
    for i in range(x - 1):
        for j in range(y):
            if newG[i][j] > newG[i + 1][j]:
                return "NO"
    return "YES"
