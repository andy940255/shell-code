# Complete the 'miniMaxSum' function below.
#
# The function accepts INTEGER_ARRAY arr as parameter.


def miniMaxSum(arr):
    arr.sort()
    s = sum(arr)
    print(s - arr[-1], s - arr[0])
