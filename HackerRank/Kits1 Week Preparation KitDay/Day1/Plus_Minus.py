def plusMinus(arr):
    L = len(arr)
    positive = 0.0
    negative = 0.0
    zeros = 0.0
    for num in arr:
        if num > 0:
            positive += 1
        elif num < 0:
            negative += 1
        else:
            zeros += 1
    print(f"{positive / L:.6f}\n{negative / L:.6f}\n{zeros / L:.6f}")
