# Complete the 'timeConversion' function below.
#
# The function is expected to return a STRING.
# The function accepts STRING s as parameter.
import time


def timeConversion(s):
    dateTime = time.strptime(s, "%I:%M:%S%p")
    return time.strftime("%H:%M:%S", dateTime)
