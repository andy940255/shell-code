from itertools import product
def queensAttack(n, k, r_q, c_q, obstacles):
    if n == 1:
        return 0
    ans = 0
    obstacles_set = set(map(tuple, obstacles))
    orient = set(product([-1, 0, 1], repeat=2)) - {(0, 0)}
    for dx, dy in orient:
        x = r_q-1
        y = c_q-1
        while 0 <= x + dx < n and 0 <= y + dy < n and (x + dx + 1, y + dy + 1) not in obstacles_set:
            ans += 1
            x += dx
            y += dy
    return ans