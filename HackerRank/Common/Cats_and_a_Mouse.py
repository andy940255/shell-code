def catAndMouse(x, y, z):
    ma = abs(z-x)
    mb = abs(z-y)
    if ma == mb:
        return "Mouse C"
    elif ma > mb:
        return "Cat B"
    else:
        return "Cat A"