# Complete the 'happyLadybugs' function below.
#
# The function is expected to return a STRING.
# The function accepts STRING b as parameter.
from collections import Counter


def happyLadybugs(b):
    counter = Counter(b)
    if any(count == 1 and char != "_" for char, count in counter.items()):
        return "NO"
    if "_" in b:
        return "YES"
    pre = None
    curHappy = None
    for cur in b:
        if pre and pre == cur:
            curHappy = True
        elif pre and pre != cur and not curHappy:
            return "NO"
        else:
            pre = cur
            curHappy = False
    return "YES"


def happyLadybugs1(b):
    counter = Counter(b)
    if any(count == 1 and char != "_" for char, count in counter.items()):
        return "NO"
    if "_" in b:
        return "YES"

    curHappy = False
    for cur, nextChar in zip(b, b[1:] + "_"):
        if cur == nextChar:
            curHappy = True
        elif cur != nextChar and not curHappy:
            return "NO"
        else:
            curHappy = False

    return "YES"
