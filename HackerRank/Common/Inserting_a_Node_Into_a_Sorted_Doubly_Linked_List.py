# For your reference:
#
# DoublyLinkedListNode:
#     int data
#     DoublyLinkedListNode next
#     DoublyLinkedListNode prev

def sortedInsert(llist, data):
    node = DoublyLinkedListNode(data)
    if llist.data >= data:
        node.next = llist
        return node
    check = False
    cur = llist
    pre = None
    while cur:
        if cur.data >= data:
            if pre:
                pre.next = node
            node.prev = pre
            node.next = cur
            cur.prev = node
            return llist
        pre = cur
        cur = cur.next
    if not check:
        pre.next = DoublyLinkedListNode(data)
        pre.next.prev = pre
    return llist