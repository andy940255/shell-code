def appendAndDelete(s, t, k):
    ls = len(s)
    lt = len(t)
    if k >= ls+lt:
        return "Yes"
    minL = min(ls, lt)
    same = 0
    for i in range(minL):
        if s[i] == t[i]:
            same += 1
        else:
            break
    k -= (ls - same)
    k -= (lt - same)
    return "Yes" if k >= 0 and k % 2 == 0 else "No"