def postOrder(root):
    res = []
    stack = []
    node = root
    while stack or node:
        if node:
            stack.append(node)
            res = [node.info] + res
            node = node.right
        else:
            node = stack.pop().left
    while res:
        print(res.pop(0), end=" ")