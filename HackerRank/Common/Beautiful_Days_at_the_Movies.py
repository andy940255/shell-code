def beautifulDays(i, j, k):
    ans = 0
    for num in range(i, j+1):
        if abs(num-int(str(num)[::-1])) % k == 0:
            ans += 1
    return ans