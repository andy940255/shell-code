from itertools import combinations_with_replacement


def stones(n, a, b):
    return sorted(
        [sum(item) for item in set(combinations_with_replacement((a, b), n - 1))]
    )


def stones1(n, a, b):
    return sorted(
        set(
            [
                (i - 1) * a + (n - i) * b for i in range(1, n + 1)
                ]
            )
        )
