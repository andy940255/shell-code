def insertNodeAtPosition(llist, data, position):
    cur = llist
    for i in range(position-1):
        cur = cur.next
    cur.next, cur.next.next = SinglyLinkedListNode(data), cur.next
    return llist