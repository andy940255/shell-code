def getMax(operations):
    ans = []
    stack = []
    maxNum = None
    appNum = None
    for op in operations:
        opsl = op.split()
        if opsl[0] == '1':
            appNum = int(opsl[1])
            stack.append(appNum)
            if maxNum and appNum > maxNum:
                maxNum = appNum
        elif opsl[0] == '2':
            if stack.pop() == maxNum:
                maxNum = None
        elif opsl[0] == '3':
            if not maxNum:
                maxNum = 0
                for num in stack:
                    if num > maxNum:
                        maxNum = num
            ans.append(maxNum)
    return ans