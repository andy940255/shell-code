def pageCount(n, p):
    fromFront = p // 2
    fromBack = (n // 2) - (p // 2)
    return min(fromFront, fromBack)