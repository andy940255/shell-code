def levelOrder(root):
    if not root:
        return
    queue = [root]
    while queue:
        cur = queue.pop(0)
        print(cur.info, end=" ")
        if cur.left:
            queue.append(cur.left)
        if cur.right:
            queue.append(cur.right)