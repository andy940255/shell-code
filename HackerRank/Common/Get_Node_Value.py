# Complete the 'getNode' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER_SINGLY_LINKED_LIST llist
#  2. INTEGER positionFromTail
#
# For your reference:
#
# SinglyLinkedListNode:
#     int data
#     SinglyLinkedListNode next

def getNode(llist, positionFromTail):
    # Write your code here
    pre = None
    cur = llist
    while cur:
        cur.next, cur, pre = pre, cur.next, cur
    
    for _ in range(positionFromTail):
        print(pre.data)
        pre = pre.next
    return pre.data