from collections import Counter
def pickingNumbers(a):
    count = Counter(a)
    max_length = 0
    for num in count:
        max_length = max(max_length, count[num] + count[num + 1])
    return max_length