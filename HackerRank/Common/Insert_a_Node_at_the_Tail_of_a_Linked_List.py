# Complete the insertNodeAtTail function below.
#
# For your reference:
#
# SinglyLinkedListNode:
#     int data
#     SinglyLinkedListNode next
#
def insertNodeAtTail(head, data):
    if head is None:
        return SinglyLinkedListNode(data)
    cur = head
    pre = None
    while cur:
        pre = cur
        cur = cur.next
    pre.next = SinglyLinkedListNode(data)
    return head