def utopianTree(n):
    H = 1
    for i in range(0, n):
        if i % 2:
            H += 1
        else:
            H *= 2
    return H