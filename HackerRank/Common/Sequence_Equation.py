def permutationEquation(p):
    ans = []
    for x in range(1, len(p)+1):
        idx = p.index(x) + 1
        ans.append(p.index(idx) + 1)
    return ans