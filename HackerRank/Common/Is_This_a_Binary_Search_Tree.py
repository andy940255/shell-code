def check_binary_search_tree_(root):
    nodes = [(root, float('-INF'), float('INF'))]
    while nodes:
        node = nodes.pop(0)
        if node[0].data <= node[1] or node[0].data >= node[2]:
            return False
        if node[0].left:
            nodes.append((node[0].left, node[1], node[0].data))
        if node[0].right:
            nodes.append((node[0].right, node[0].data, node[2]))
    return True