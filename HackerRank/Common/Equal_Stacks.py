def equalStacks(h1, h2, h3):
    hStack = [h1, h2, h3]
    sumStack = [sum(h1), sum(h2), sum(h3)]
    if 0 in sumStack:
        return 0
    while len(set(sumStack)) > 1 and 0 not in sumStack:
        mMaxIdx = sumStack.index(max(sumStack))
        sumStack[mMaxIdx] -= hStack[mMaxIdx].pop(0)
    return min(sumStack)