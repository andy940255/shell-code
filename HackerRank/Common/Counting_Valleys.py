def countingValleys(steps, path):
    ans = 0
    level = 0
    isLow = False
    for step in path:
        if level < 0:
            isLow = True
        if step == "D":
            level -= 1
        elif step == "U":
            level += 1
        if isLow and level == 0:
            isLow = False
            ans += 1
    return ans