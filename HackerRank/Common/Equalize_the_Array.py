from collections import Counter
def equalizeArray(arr):
    return len(arr) - Counter(arr).most_common(1)[0][1]