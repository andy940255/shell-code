def viralAdvertising(n):
    ans= 0
    num = 5
    for i in range(n):
        share = num // 2
        ans += share
        num = share * 3
    return ans