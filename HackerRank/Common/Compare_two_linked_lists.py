def compare_lists(llist1, llist2):
    cur1, cur2 = llist1, llist2
    while cur1 or cur2:
        if cur1 is None or cur2 is None:
            return False
        if cur1.data != cur2.data:
            return False
        cur1 = cur1.next
        cur2 = cur2.next
    return True