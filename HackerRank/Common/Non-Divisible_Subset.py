def nonDivisibleSubset(k, s):
    remain = [0] * k
    for i in s:
        remain[i % k] += 1
    ans = 1 if not k % 2 else 0
    ans += min(remain[0], 1)
    for i in range(1, k//2+1):
        if i != k-i:
            ans += max(remain[i], remain[k-i])
    return ans