def findDigits(n):
    ans = 0
    for sNum in str(n):
        if sNum != "0" and n % int(sNum) == 0:
            ans += 1
    return ans