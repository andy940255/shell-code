#!/bin/python3

import math
import os
import random
import re
import sys
from collections import Counter


if __name__ == '__main__':
    counter = Counter(input())
    arr = counter.most_common()
    arr = sorted(arr, key = lambda ele: ele[0])
    arr = sorted(arr, key = lambda ele: ele[1], reverse=True)
    for i, j in arr[0:3]:
        print(i, j)
