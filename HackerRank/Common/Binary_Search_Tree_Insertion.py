#Node is defined as
#self.left (the left child of the node)
#self.right (the right child of the node)
#self.info (the value of the node)


def insert(self, val):
    if self.root == None:
        self.root = Node(val)
    else:
        cur = self.root
        while cur:
            if cur.info > val:
                if cur.left:
                    cur = cur.left
                else:
                    cur.left = Node(val)
                    return
            else:
                if cur.right:
                    cur = cur.right
                else:
                    cur.right = Node(val)
                    return