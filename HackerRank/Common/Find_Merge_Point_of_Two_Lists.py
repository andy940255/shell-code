def findMergeNode(head1, head2):
    nodes_set = set()
    while head1:
        nodes_set.add(head1)
        head1 = head1.next
    while head2:
        if head2 in nodes_set:
            return head2.data
        head2 = head2.next
    return -1