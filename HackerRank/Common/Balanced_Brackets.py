def isBalanced(s):
    stack = []
    left = ["[", "{", "("]
    right = ["]", "}", ")"]
    for bk in s:
        if bk in left:
            stack.append(bk)
        elif bk in right:
            if stack:
                idx = right.index(bk)
                if left[idx] != stack.pop():
                    return "NO"
            else:
                return "NO"
        else:
            return "NO"
    return "YES" if not stack else "NO"