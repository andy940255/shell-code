def repeatedString(s, n):
    return math.floor((n)/len(s))*s.count('a') + s[:(n%len(s))].count('a')