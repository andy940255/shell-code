# Complete the has_cycle function below.
#
# For your reference:
#
# SinglyLinkedListNode:
#     int data
#     SinglyLinkedListNode next
#
def has_cycle(head):
    fast, low = head, head
    while fast.next and fast.next.next:
        fast = fast.next.next
        low = low.next
        if fast == low:
            return 1
    return 0