# Complete the 'removeDuplicates' function below.
#
# The function is expected to return an INTEGER_SINGLY_LINKED_LIST.
# The function accepts INTEGER_SINGLY_LINKED_LIST llist as parameter.
#
# For your reference:
#
# SinglyLinkedListNode:
#     int data
#     SinglyLinkedListNode next

def removeDuplicates(llist):
    pre = None
    cur = llist
    while cur:
        if pre is not None and cur.data == pre.data:
            pre.next = cur.next
        else:
            pre = cur
        cur = cur.next
    return llist