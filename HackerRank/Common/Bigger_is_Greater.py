# Complete the 'biggerIsGreater' function below.
#
# The function is expected to return a STRING.
# The function accepts STRING w as parameter.
#


def biggerIsGreater(s):
    # Convert the string to a list for easy manipulation
    s = list(s)

    # Find the rightmost character that is smaller than the character to its right
    i = len(s) - 2
    while i >= 0 and s[i] >= s[i + 1]:
        i -= 1

    # If no such character is found, the string is the last permutation
    if i == -1:
        return "no answer"

    # Find the smallest character to the right of i that is larger than s[i]
    j = len(s) - 1
    while s[j] <= s[i]:
        j -= 1

    # Swap the characters at positions i and j
    s[i], s[j] = s[j], s[i]

    # Reverse the substring to the right of i
    s[i + 1 :] = reversed(s[i + 1 :])

    # Convert the list back to a string
    result = "".join(s)

    return result
