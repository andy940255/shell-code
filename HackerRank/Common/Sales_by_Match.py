def sockMerchant(n, ar):
    pair = 0
    check = []
    for sock in ar:
        if sock not in check:
            check.append(sock)
        else:
            check.remove(sock)
            pair += 1
    return pair