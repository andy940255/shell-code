#!/bin/python3
from datetime import datetime
import math
import os
import random
import re
import sys

# Complete the time_delta function below.
def time_delta(t1, t2):
    t1 = t1.split(" ")
    t2 = t2.split(" ")
    time_format = "%Y %b %d %H:%M:%S %z"
    t1_str = "{} {} {} {} {}".format(t1[3], t1[2], t1[1], t1[4], t1[5])
    t2_str = "{} {} {} {} {}".format(t2[3], t2[2], t2[1], t2[4], t2[5])
    time1 = datetime.strptime(t1_str, time_format)
    time2 = datetime.strptime(t2_str, time_format)
    ans = (time1-time2)
    return str(abs(int(ans.total_seconds())))

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    t = int(input())

    for t_itr in range(t):
        t1 = input()

        t2 = input()

        delta = time_delta(t1, t2)

        fptr.write(delta + '\n')

    fptr.close()
