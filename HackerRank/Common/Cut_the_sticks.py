from collections import Counter
def cutTheSticks(arr):
    ans = []
    L = len(arr)
    counter = Counter(arr)
    arr = sorted(counter.keys())
    for i in arr:
        ans.append(L)
        L -= counter[i]
    return ans