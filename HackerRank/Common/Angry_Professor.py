def angryProfessor(k, a):
    ans = 0
    for i in a:
        if i <= 0:
            ans += 1 
    return "YES" if ans < k else "NO"

print("".join( str(_) for _ in range(10)))