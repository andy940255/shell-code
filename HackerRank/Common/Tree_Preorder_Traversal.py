def preOrder(root):
    stack = [root]
    while stack:
        node = stack.pop()
        if node:
            print(node.info, end=" ")
            stack.append(node.right)
            stack.append(node.left)