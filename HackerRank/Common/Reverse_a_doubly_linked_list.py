def reverse(llist):
    tail = None
    while llist:
        llist.next, llist.prev, tail, llist = llist.prev, llist, llist, llist.next
    return tail