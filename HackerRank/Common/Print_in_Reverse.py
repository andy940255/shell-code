def reversePrint(llist):
    pre = None
    cur = llist
    while cur:
        # temp = cur
        # cur = cur.next
        # temp.next = pre
        # pre = temp
        # cur.next, pre, cur = pre, cur, cur.next
        cur.next, cur, pre = pre, cur.next, cur
    while pre:
        print(pre.data)
        pre = pre.next