# Complete the 'gridSearch' function below.
#
# The function is expected to return a STRING.
# The function accepts following parameters:
#  1. STRING_ARRAY G
#  2. STRING_ARRAY P


def gridSearch(G, P):
    LG = len(G)
    LP = len(P)
    for i in range(LG):
        idx = 0
        findStart = []
        while True:
            f = G[i].find(P[0], idx)
            if f == -1:
                break
            findStart.append(f)
            idx = f + 1
        for findS in findStart:
            check = True
            for j in range(1, LP):
                if i + j >= LG:
                    check = False
                    break
                idx = 0
                findNext = []
                while True:
                    fn = G[i + j].find(P[j], idx)
                    if fn == -1:
                        break
                    findNext.append(fn)
                    idx = fn + 1
                if findS not in findNext:
                    check = False
                    break
            if check:
                return "YES"
    return "NO"
