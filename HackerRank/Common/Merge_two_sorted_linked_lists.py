# Complete the mergeLists function below.
#
# For your reference:
#
# SinglyLinkedListNode:
#     int data
#     SinglyLinkedListNode next

def mergeLists(head1, head2):
    ans = SinglyLinkedListNode(None)
    cur = ans
    while head1 or head2:
        if head1 and head2:
            if head1.data <= head2.data:
                cur.next = head1
                head1 = head1.next
            else:
                cur.next = head2
                head2 = head2.next
        elif head1:
            cur.next = head1
            head1 = head1.next
        elif head2:
            cur.next = head2
            head2 = head2.next
        cur = cur.next
    return ans.next