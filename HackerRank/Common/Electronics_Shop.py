def getMoneySpent(keyboards, drives, b):
    keyboards.sort(reverse=True)
    drives.sort()
    max_spent = -1
    keyboard_index = 0
    drive_index = 0
    while keyboard_index < len(keyboards) and drive_index < len(drives):
        total = keyboards[keyboard_index] + drives[drive_index]
        if total <= b and total > max_spent:
            max_spent = total
        if total > b:
            keyboard_index += 1
        else:
            drive_index += 1
    return max_spent