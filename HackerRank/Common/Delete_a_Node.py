def deleteNode(llist, position):
    if position == 0:
        return llist.next
    cur = llist
    for i in range(position-1):
        cur = cur.next
    cur.next = cur.next.next
    return llist