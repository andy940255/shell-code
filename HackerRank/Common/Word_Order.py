# Enter your code here. Read input from STDIN. Print output to STDOUT
nums = int(input())
dic = {}
for strr in range(nums):
    inp = input()
    if inp in dic:
        dic[inp] += 1
    else:
        dic[inp] = 1
print(len(dic))
for key, val in dic.items():
    print(val, end = " ")
