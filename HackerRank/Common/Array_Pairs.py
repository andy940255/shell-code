import math
import os
import random
import re
import sys

# Complete the 'solve' function below.
#
# The function is expected to return a LONG_INTEGER.
# The function accepts INTEGER_ARRAY arr as parameter.

# def solve(arr):
#     ans = 0
#     L = len(arr)
#     for i in range(L):
#         if arr[i] == 1:
#             ans += L - 1 - i
#             continue
#         for j in range(i+1, L):
#             mNum = max(arr[i:j+1])
#             if arr[i] <= mNum / arr[j]:
#                 ans += 1
#     return ans

def solve(arr):
    ans = 0
    L = len(arr)
    for i in range(L-1):
        if arr[i] == 1:
            ans += L - 1 - i
            continue
        mMap = []
        sMap = []
        for j in range(i+1, L):
            mMap.append(max(arr[i:j+1]))
            sMap.append(arr[i] * arr[j])
            if sMap[-1] <= mMap[-1]:
                ans += 1
    return ans

if __name__ == '__main__':
    arr_count = int(input().strip())
    arr = list(map(int, input().rstrip().split()))
    result = solve(arr)
    print(result)