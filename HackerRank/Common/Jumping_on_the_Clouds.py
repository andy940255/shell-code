def jumpingOnClouds(c):
    idx = 0
    step = 0
    L = len(c)
    while idx < L-1:
        if idx+2 < L and c[idx+2] == 0:
            idx += 2
        else:
            idx += 1
        step += 1
    return step