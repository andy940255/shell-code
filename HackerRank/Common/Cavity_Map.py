def cavityMap(grid):
    ans = []
    n = len(grid)
    for i in range(0, n):
        temp = ""
        for j in range(0, n):
            cur = grid[i][j]
            if i == 0 or i == n - 1 or j == 0 or j == n - 1:
                temp += str(cur)
            elif (
                grid[i + 1][j] >= cur
                or grid[i - 1][j] >= cur
                or grid[i][j + 1] >= cur
                or grid[i][j - 1] >= cur
            ):
                temp += str(cur)
            else:
                temp += "X"
        ans.append(temp)
    return ans
