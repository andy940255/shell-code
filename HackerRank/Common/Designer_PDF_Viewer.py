def designerPdfViewer(h, word):
    maxH = 0
    for i in word:
        maxH = max(h[ord(i)-ord('a')], maxH)
    return maxH * len(word)