# Complete the 'reverse' function below.
#
# The function is expected to return an INTEGER_SINGLY_LINKED_LIST.
# The function accepts INTEGER_SINGLY_LINKED_LIST llist as parameter.
#
# For your reference:
#
# SinglyLinkedListNode:
#     int data
#     SinglyLinkedListNode next

def reverse(llist):
    cur = llist
    pre = None
    while cur:
        # temp = cur
        # cur = cur.next
        # temp.next = pre
        # pre = temp
        
        # temp = cur.next
        # cur.next = pre
        # pre = cur
        # cur = temp
        
        cur.next, cur, pre = pre, cur.next, cur
        # O cur.next, pre, cur = pre, cur, cur.next
        # O pre, cur.next, cur = cur, pre, cur.next
        # X cur, cur.next, pre = cur.next, pre, cur
        # X cur, pre, cur.next = cur.next, cur, pre
        # X pre, cur, cur.next = cur, cur.next, pre

    return pre