def height(root):
    def H(node):
        if not node:
            return 0
        else:
            left_height = H(node.left)
            right_height = H(node.right)
            res = max(left_height, right_height)
            return res + 1
    return H(root) - 1