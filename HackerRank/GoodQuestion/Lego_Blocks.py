# Complete the 'legoBlocks' function below.
#
# The function is expected to return an INTEGER.
# The function accepts following parameters:
#  1. INTEGER n
#  2. INTEGER m
#


# From https://www.youtube.com/watch?v=u9fL52Hryiw
def legoBlocks(h, w):
    module = 10**9 + 7
    row = [0, 1, 2, 4, 8] + [0 for _ in range(w - 4)]
    for i in range(5, w + 1):
        row[i] = (row[i - 1] + row[i - 2] + row[i - 3] + row[i - 4]) % module

    total = row.copy()
    for _ in range(2, h + 1):
        for i in range(w + 1):
            total[i] = (row[i] * total[i]) % module

    solid = [0 for _ in range(w + 1)]
    solid[1] = 1
    for ww in range(2, w + 1):
        unsolid = 0
        for i in range(1, ww):
            unsolid += (solid[i] * total[ww - i]) % module
        solid[ww] = (total[ww] - unsolid) % module
    return solid[w] % module
