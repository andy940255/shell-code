#!/bin/python3
# Failed Problem
import heapq
import math
import os
import random
import re
import sys

#
# Complete the 'roadsInHackerland' function below.
#
# The function is expected to return a STRING.
# The function accepts following parameters:
#  1. INTEGER n
#  2. 2D_INTEGER_ARRAY roads
#
    
def roadsInHackerland(n, roads):




# def roadsInHackerland(n, roads):
#     mapp = [[float("INF")] * n for _ in range(n)]
#     for i in range(n):
#         mapp[i][i] = 0
#     for road in roads:
#         mapp[road[0] - 1][road[1] - 1] = 2 ** road[2]
#         mapp[road[1] - 1][road[0] - 1] = 2 ** road[2]

#     book = [[0] * n for _ in range(n)]
#     ans = 0
#     for N in range(n):
#         book[N][0] = 1
#         for i in range(n-1):
#             min = float("INF")
#             for j in range(n):
#                 if book[N][j] == 0 and mapp[N][j] < min:
#                     min = mapp[N][j]
#                     u = j
#             book[N][u] = 1
#             for v in range(n):
#                 if mapp[u][v] < float("INF"):
#                     if mapp[N][v] > mapp[N][u] + mapp[u][v]:
#                         mapp[N][v] = mapp[N][u] + mapp[u][v]
#         ans += sum(mapp[N][N:])
    
#     return bin(ans)[2:]
    pass

"""
5 6
1 3 5
4 5 0
2 1 3
3 2 1
4 3 4
4 2 2
"""
if __name__ == "__main__":
    # fptr = open(os.environ['OUTPUT_PATH'], 'w')

    first_multiple_input = input().rstrip().split()

    n = int(first_multiple_input[0])

    m = int(first_multiple_input[1])

    roads = []

    for _ in range(m):
        roads.append(list(map(int, input().rstrip().split())))

    result = roadsInHackerland(n, roads)

    print(result)

    # fptr.write(result + '\n')

    # fptr.close()


# 我寫不出來
    
    from collections import defaultdict

n, m = map(int, input().split())

edges = []
parent = [i for i in range(n)]

for _ in range(m):
	a, b, c = map(int, input().split())
	edges.append((a-1, b-1, c))

print(edges)
print(parent)

def find(i):
	print(f"i: {i}, parent: {parent}")
	while i != parent[parent[i]]:
		parent[i] = parent[parent[i]]
		i = parent[i]
	return i 

def union(x, y):
	p_x = find(x)
	p_y = find(y)
	parent[p_y] = p_x

def is_connected(x, y):
	p_x = find(x)
	p_y = find(y)
	return p_x == p_y

# build MST
tree = defaultdict(list)

edges.sort(key = lambda x:x[2])
for a, b, c in edges:
	if not is_connected(a, b):
		union(a, b)
		tree[a].append((b, c))
		tree[b].append((a, c))



print(tree)
print(edges)
print(parent)
ans = [0]*(2*m)
print(ans)

# Run DFS to count the number of times an edge is used
# as weights of all edges is different, hence each weight maps to a particular children
def dfs(src, p = -1):
	total = 1
	for v, c in tree[src]:
		if v != p:
			children = dfs(v, src)
			
			# children => nodes right to edge, n - children => nodes left to edge
			ans[c] += (n - children)*children	
			
			total += children
	return total

dfs(0)
print(ans)

res = 0
for i in range(len(ans)):
	res += ans[i] * (1 << i)

print(str(bin(res))[2:])

"""
5 6
1 3 5
4 5 0
2 1 3
3 2 1
4 3 4
4 2 2
"""