def topView(root):
    if not root:
        return
    dis = {}
    queue = [(root, 0)]

    while queue:
        cur, d = queue.pop(0)
        if d not in dis:
            dis[d] = cur.info
        if cur.left:
            queue.append((cur.left, d - 1))
        if cur.right:
            queue.append((cur.right, d + 1))
    for key in sorted(dis):
        print(dis[key], end=" ")