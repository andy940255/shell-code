class Solution:
    def __init__(self):
        self.stack = []
        self.queue = []
        
    def pushCharacter(self, s):
        self.stack.append(s)
        
    def popCharacter(self):
        return self.stack.pop()
        
    def enqueueCharacter(self, s):
        self.queue.append(s)
        
    def dequeueCharacter(self):
        return self.queue.pop(0)