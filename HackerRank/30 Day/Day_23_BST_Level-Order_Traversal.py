def levelOrder(self, root):
    stack = [root]
    while stack:
        node = stack.pop(0)
        if node:
            print(node.data, end=" ")
            stack.append(node.left)
            stack.append(node.right)
