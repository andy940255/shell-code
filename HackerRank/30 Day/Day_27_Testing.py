class TestDataEmptyArray:
    def get_array():
        return []


class TestDataUniqueValues:
    def get_array():
        return [9, 1]

    def get_expected_result():
        return 1


class TestDataExactlyTwoDifferentMinimums:
    def get_array():
        return [9, 1, 1]

    def get_expected_result():
        return 1
