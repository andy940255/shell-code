def getHeight(self, root):
    def H(node):
        if not node:
            return 0
        return max(H(node.left), H(node.right)) + 1

    return H(root) - 1
