def bitwiseAnd(N, K):
    ans = float("-INF")
    for i in range(1, N):
        for j in range(i + 1, N + 1):
            if (i & j) > ans and (i & j) < K:
                ans = i & j
    return ans
