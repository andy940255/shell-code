class Solution:
    def sumSubarrayMins(self, arr) -> int:
        ans = 0
        n = len(arr)
        for i in range(n):
            minNum = float("inf")
            for j in range(i, n):
                minNum = min(minNum, arr[j])
                ans += minNum
        return ans % (10**9 + 7)


class Solution:
    def sumSubarrayMins1(self, arr) -> int:
        ans = 0
        stack = []
        arr = [float("-inf")] + arr + [float("-inf")]
        for i in range(len(arr)):
            while stack and arr[i] < arr[stack[-1]]:
                j = stack.pop()
                k = stack[-1]
                ans += arr[j] * (i - j) * (j - k)
            stack.append(i)

        return ans % (10**9 + 7)

    def sumSubarrayMins2(self, arr) -> int:
        ans = 0
        stack = [-1]
        arr = arr + [-1]
        for i in range(len(arr)):
            while arr[i] < arr[stack[-1]]:
                j = stack.pop()
                k = stack[-1]
                ans += arr[j] * (i - j) * (j - k)
            stack.append(i)

        return ans % (10**9 + 7)
