class Solution:
    def insert(
        self, intervals: List[List[int]], newInterval: List[int]
    ) -> List[List[int]]:
        idx = 0
        n = len(intervals)
        ans = []
        while idx < n:
            if newInterval[0] < intervals[idx][0]:
                break
            elif intervals[idx][0] <= newInterval[0] <= intervals[idx][1]:
                newInterval[0] = intervals[idx][0]
                break
            else:
                ans.append(intervals[idx])
                idx += 1

        while idx < n:
            if newInterval[1] < intervals[idx][0]:
                break
            elif intervals[idx][0] <= newInterval[1] <= intervals[idx][1]:
                newInterval[1] = intervals[idx][1]
                idx += 1
                break
            else:
                idx += 1

        ans.append(newInterval)

        while idx < n:
            ans.append(intervals[idx])
            idx += 1

        return ans

    def insert(
        self, intervals: List[List[int]], newInterval: List[int]
    ) -> List[List[int]]:
        res = []

        for i, n in enumerate(intervals):
            if newInterval[1] < n[0]:
                res.append(newInterval)
                return res + intervals[i:]

            elif newInterval[0] > n[1]:
                res.append(n)

            else:
                newInterval = [min(newInterval[0], n[0]), max(newInterval[1], n[1])]

        res.append(newInterval)

        return res
