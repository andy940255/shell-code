class Solution:
    def maxLength(self, A):
        dp = [set()]
        for a in A:
            sa = set(a)
            if len(sa) < len(a):
                continue
            for c in dp[:]:
                if sa & c:
                    continue
                dp.append(sa | c)
        return max(len(sa) for sa in dp)
