class Solution:
    def gridGame(self, grid: List[List[int]]) -> int:
        n = len(grid[0])
        for i in range(1, n):
            grid[0][i] += grid[0][i - 1]
            grid[1][i] += grid[1][i - 1]

        ans = math.inf
        for i in range(n):
            max1 = grid[0][-1] - grid[0][i]
            if i > 0:
                max1 = max(max1, grid[1][i - 1])
            ans = min(ans, max1)
        return ans

    def gridGame(self, grid: List[List[int]]) -> int:
        first, second = sum(grid[0][1:]), 0
        res = first
        for i in range(1, len(grid[0])):
            first -= grid[0][i]
            second += grid[1][i - 1]
            if first <= second:
                if res > second:
                    res = second
                break
            res = first
        return res
