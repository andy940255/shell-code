import sys

class Solution:
    def kInversePairs1(self, n: int, k: int) -> int:
        # Time Out
        mapp = {}
        mapp[(1, 1)] = 0
        def solve(num, key):
            # print(num, key)
            if (num, key) in mapp:
                # print(f"{num}, {key} in map")
                return mapp[(num, key)]
            if k > n * (n-1)//2:
                mapp[(num, key)] = 0
                return 0
            if num <= 0:
                mapp[(num, key)] = 0
                return 0
            if key == 0:
                mapp[(num, key)] = 1
                return 1
            sumNum = 0

            for i in range(max(key-num+1, 0), key+1): 
                sumNum += solve(num-1, i)
            mapp[(num, key)] = sumNum
            return sumNum
        ans = solve(n, k)
        # print(mapp)
        return ans % (10**9+7)

    def kInversePairs(self, n: int, k: int) -> int:
        max_possible_inversions = n * (n - 1) // 2
        if k > max_possible_inversions:
            return 0
        if k == 0 or k == max_possible_inversions:
            return 1
        mod = 10**9+7
        dp = [[0] * (k + 1) for _ in range(n + 1)]

        for i in range(1, n + 1):
            dp[i][0] = 1

        for i in range(2, n + 1):
            max_possible_inversions = min(k, (i * (i - 1) // 2))
            for j in range(1, max_possible_inversions + 1):
                # dp[i][j] = (mod + sum([dp[i - 1][x] for x in range(max(j - i + 1, 0), min(j, ((i-1) * (i - 2) // 2))+1)])) % mod
                dp[i][j] = dp[i][j-1] + dp[i-1][j]
                if j>=i:
                    dp[i][j] -= dp[i-1][j - i]
                dp[i][j] %= mod
        return dp[n][k]


S = Solution()
print(S.kInversePairs(100, 100))