class Solution:
    def subarraySum(self, nums, k):
        ans = 0
        sumNum = 0
        sumMap = {0: 1}
        for num in nums:
            sumNum += num
            ans += sumMap.get(sumNum - k, 0)
            sumMap[sumNum] = sumMap.get(sumNum, 0) + 1
        return ans
