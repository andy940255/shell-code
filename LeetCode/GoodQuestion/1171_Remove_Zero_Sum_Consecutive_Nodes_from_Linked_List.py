# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def removeZeroSumSublists(self, head: Optional[ListNode]) -> Optional[ListNode]:
        transfer = []
        while head:
            transfer.append(head.val)
            head = head.next

        stack = []
        while transfer:
            node = transfer.pop()
            if node == 0:
                continue
            n = len(stack)
            tempSum = 0
            count = 0
            while count < n:
                tempSum += stack[n - 1 - count]
                count += 1
                if tempSum + node == 0:
                    for i in range(count):
                        stack.pop()
                    break
            if tempSum + node != 0:
                stack.append(node)
        Head = ListNode()
        cur = Head
        while stack:
            cur.next = ListNode(stack.pop())
            cur = cur.next
        return Head.next

    def removeZeroSumSublists(self, head: Optional[ListNode]) -> Optional[ListNode]:
        front = ListNode(0, head)
        current = front
        prefix_sum = 0
        prefix_sums = {0: front}
        # Calculate the prefix sum for each node and add to the hashmap
        # Duplicate prefix sum values will be replaced
        while current:
            prefix_sum += current.val
            prefix_sums[prefix_sum] = current
            current = current.next
        # Reset prefix sum and current
        prefix_sum = 0
        current = front
        # Delete zero sum consecutive sequences by setting node before sequence to node after
        while current:
            prefix_sum += current.val
            current.next = prefix_sums[prefix_sum].next
            current = current.next
        return front.next
