from collections import defaultdict
from itertools import combinations


class Solution:
    def numberOfArithmeticSlices(self, nums: list[int]) -> int:
        # My Time Out Solution
        ans = 0
        checkList = []
        ansList = []
        for i in range(3, len(nums) + 1):
            checkList += list(combinations(nums, i))
        for arr in checkList:
            if self.check1(arr):
                ans += 1
                ansList.append(arr)
        return ans

    def check1(self, arr):
        gap = arr[1] - arr[0]
        for i in range(2, len(arr)):
            if arr[i] - arr[i - 1] != gap:
                return False
        return True

    def numberOfArithmeticSlices1(self, nums: list[int]) -> int:
        # Good Solution
        ans = 0
        n = len(nums)
        dp = [defaultdict(int) for _ in range(n)]

        for i in range(1, n):
            for j in range(i):
                diff = nums[i] - nums[j]
                cnt = 0
                if diff in dp[j]:
                    cnt = dp[j][diff]
                dp[i][diff] += cnt + 1
                ans += cnt
        return ans

    def numberOfArithmeticSlices2(self, nums):
        # Good Solution
        n = len(nums)
        ans = 0

        dp = [[0] * n for _ in range(n)]
        numToIndices = {}
        for i in range(n):
            numToIndices.setdefault(nums[i], []).append(i)
        for i in range(n):
            for j in range(i):
                target = nums[j] * 2 - nums[i]
                if target in numToIndices:
                    for k in numToIndices[target]:
                        if k < j:
                            dp[i][j] += dp[j][k] + 1
                ans += dp[i][j]
        return ans


# nums = [7, 7, 7, 7, 7]
# nums = [2, 4, 6, 8, 10]
import time
nums = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

S = Solution()
a = time.time()
print(S.numberOfArithmeticSlices(nums))
b = time.time()
print(b-a)
print(S.numberOfArithmeticSlices1(nums))
c = time.time()
print(c-b)
print(S.numberOfArithmeticSlices2(nums))
d = time.time()
print(d-c)