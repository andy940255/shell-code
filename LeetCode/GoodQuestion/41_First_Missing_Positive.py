class Solution:
    def firstMissingPositive(self, nums: List[int]) -> int:
        contains_1 = False
        n = len(nums)
        for i in range(n):
            if nums[i] == 1:
                contains_1 = True
            if nums[i] <= 0 or nums[i] > n:
                nums[i] = 1

        if not contains_1:
            return 1

        for i in range(n):
            num = abs(nums[i])

            if num == n:
                nums[0] = -1 * abs(nums[0])
            else:
                nums[num] = -1 * abs(nums[num])

        for i in range(1, n):
            if nums[i] > 0:
                return i

        if nums[0] > 0:
            return n
        else:
            return n + 1
