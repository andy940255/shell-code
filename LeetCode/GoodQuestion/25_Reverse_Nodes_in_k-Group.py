# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def reverseKGroup(self, head, k: int):
        length = 0
        cur = head
        while cur:
            cur = cur.next
            length += 1

        loop = length // k
        H = None
        cur = head

        pre = None
        tail0 = None
        tail1 = cur
        for loo in range(loop):
            if loo != 0:
                tail0 = tail1
                pre = tail0
                tail1 = cur

            for step in range(k):
                cur.next, cur, pre = pre, cur.next, cur

            if loo == 0:
                H = pre
            else:
                tail0.next = pre
            tail1.next = cur

        return H
