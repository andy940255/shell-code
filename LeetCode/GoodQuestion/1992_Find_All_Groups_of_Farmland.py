class Solution:
    def findFarmland(self, land: List[List[int]]) -> List[List[int]]:
        ans = []
        m = len(land)
        n = len(land[0])

        def dfs(x, y):
            if land[x][y] == 0:
                return
            a, b = 0, 0
            while x + a < m and land[x + a][y] == 1:
                a += 1
            while y + b < n and land[x][y + b] == 1:
                for i in range(x, x + a):
                    land[i][y + b] = 0
                b += 1
            ans.append([x, y, x + a - 1, y + b - 1])

        for i in range(m):
            for j in range(n):
                dfs(i, j)
        return ans
