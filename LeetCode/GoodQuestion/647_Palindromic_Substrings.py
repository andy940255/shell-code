class Solution:
    def countSubstrings(self, s: str) -> int:
        def check(substr):
            n = len(substr)
            for i in range(n):
                if substr[i] != substr[n-i-1]:
                    return False
            return True

        ans = 0
        length = 1
        sLen = len(s)
        while length <= sLen:
            for i in range(sLen - length + 1):
                if check(s[i:i+length]):
                    ans += 1
            length += 1
        return ans
