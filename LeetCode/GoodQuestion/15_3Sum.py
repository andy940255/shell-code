class Solution:
    # My Slow Soltion
    def threeSum(self, nums: List[int]) -> List[List[int]]:
        nums.sort()
        ans = set()
        n = len(nums)
        for k in range(n):
            if nums[k] > 0 or k == n - 1:
                break
            target = nums[k] * -1
            numMap = {}
            for i in range(k + 1, n):
                diff = target - nums[i]
                if diff in numMap:
                    ans.add((nums[k], nums[i], nums[numMap[diff]]))
                numMap[nums[i]] = i

        return ans


class Solution:
    def threeSum(self, nums: List[int]) -> List[List[int]]:
        nums.sort()
        ans = set()
        n = len(nums)
        for idx, ele in enumerate(nums):
            if ele > 0:
                break
            if idx > 0 and ele == nums[idx - 1]:
                continue

            start, end = idx + 1, n - 1
            while start < end:
                sum3 = ele + nums[start] + nums[end]

                if sum3 < 0:
                    start += 1
                elif sum3 > 0:
                    end -= 1
                elif sum3 == 0:
                    ans.add((ele, nums[start], nums[end]))
                    start += 1
                    end -= 1
        return ans
