# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def reorderList(self, head: Optional[ListNode]) -> None:
        """
        Do not return anything, modify head in-place instead.
        """
        length = 0
        cur = head
        while cur:
            cur = cur.next
            length += 1
        if length < 3:
            return head
        left = head
        right = None

        cur = head
        tail = None
        for i in range(length // 2):
            tail = cur
            cur = cur.next
        tail.next = None
        while cur:
            cur.next, cur, right = right, cur.next, cur

        H = ListNode(-1)
        cur = H
        while left or right:
            if left:
                cur.next = left
                left = left.next
                cur = cur.next
            if right:
                cur.next = right
                right = right.next
                cur = cur.next
        return H

    def reorderList(self, head: Optional[ListNode]) -> None:
        # Timeout Solution
        pre = None
        cur = head

        H = ListNode(-1)
        H.next = head

        while cur:
            nex = cur.next
            pre = None
            while nex:
                nex.next, nex, pre = pre, nex.next, nex
            cur.next = pre
            cur = cur.next
        return H

    # From @DBabichev
    def reorderList(self, head: Optional[ListNode]) -> None:
        # step 1: find middle
        if not head:
            return []

        slow, fast = head, head
        while fast.next and fast.next.next:
            slow = slow.next
            fast = fast.next.next

        # step 2: reverse second half
        prev, curr = None, slow.next
        while curr:
            nextt = curr.next
            curr.next = prev
            prev = curr
            curr = nextt
        slow.next = None
        # step 3: merge lists
        head1, head2 = head, prev
        while head2:
            nextt = head1.next
            head1.next = head2
            head1 = head2
            head2 = nextt
