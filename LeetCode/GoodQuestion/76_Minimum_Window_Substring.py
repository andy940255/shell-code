from collections import Counter


class Solution:
    def minWindow(self, s: str, t: str) -> str:
        sCounter = Counter(s)
        tCounter = Counter(t)
        if sCounter & tCounter < tCounter or len(s) < len(t):
            return ""
        n = len(s)
        ans = (0, n)
        left = 0
        right = 0
        window = Counter()

        while True:
            # 左指標往右尋找第一個
            while left < n and s[left] not in t:
                left += 1
            if left >= n:
                break

            # 右指標往右擴展直到包含所有字母
            while right < n and window & tCounter < tCounter:
                if s[right] in t:
                    window[s[right]] += 1
                right += 1
            if right > n or window & tCounter < tCounter:
                break

            if right - left < ans[1] - ans[0]:
                ans = (left, right)
            if s[left] in window:
                window[s[left]] -= 1
            left += 1

        return s[ans[0] : ans[1]]


S = Solution()
s = "ADOBECODEBANC"
t = "ABC"
print(S.minWindow(s, t))
