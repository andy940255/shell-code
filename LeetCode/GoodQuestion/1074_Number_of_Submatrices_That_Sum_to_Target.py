class Solution:
    def numSubmatrixSumTarget(self, matrix, target):
        x, y = len(matrix), len(matrix[0])

        for row in matrix:
            for i in range(y - 1):
                row[i + 1] += row[i]

        ans = 0
        for i in range(y):
            for j in range(i, y):
                cur = 0
                c = {0: 1}
                for k in range(x):
                    cur += matrix[k][j] - (matrix[k][i - 1] if i > 0 else 0)
                    ans += c.get(cur - target, 0)
                    c[cur] = c.get(cur, 0) + 1
        return ans


matrix = [[0, 1, 0], [1, 1, 1], [0, 1, 0]]
target = 0

S = Solution()
print(S.numSubmatrixSumTarget(matrix, target))
