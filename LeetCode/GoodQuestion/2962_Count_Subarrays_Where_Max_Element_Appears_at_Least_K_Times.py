class Solution:
    def countSubarrays(self, nums: List[int], k: int) -> int:
        n = len(nums)
        if n < k:
            return 0
        maxNum = max(nums)
        if nums.count(maxNum) < k:
            return 0

        ans = 0
        left = 0
        count = 0
        for right in range(n):
            count += nums[right] == maxNum

            while count >= k:
                ans += n - right
                count -= nums[left] == maxNum
                left += 1
        return ans
