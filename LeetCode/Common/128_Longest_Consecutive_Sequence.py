class Solution:
    def longestConsecutive(self, nums: List[int]) -> int:
        ans = 0
        lenght = 0
        cur = float("-inf")

        for num in sorted(set(nums)):
            if cur + 1 == num:
                cur = num
                lenght += 1
            else:
                ans = max(ans, lenght)
                cur = num
                lenght = 1
        return max(ans, lenght)
