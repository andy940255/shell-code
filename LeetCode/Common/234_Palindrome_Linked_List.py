# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def isPalindrome(self, head: Optional[ListNode]) -> bool:
        slow, fast, pre = head, head, None
        while fast and fast.next:
            slow = slow.next
            fast = fast.next.next

        pre = slow
        slow = slow.next
        pre.next = None

        temp = None
        while slow:
            temp = slow.next
            slow.next = pre
            pre = slow
            slow = temp
        left = head
        right = pre

        while right:
            if left.val != right.val:
                return False
            left = left.next
            right = right.next
        return True

    def isPalindrome(self, head: Optional[ListNode]) -> bool:
        rev, slow, fast = None, head, head

        while fast and fast.next:
            fast = fast.next.next
            rev, rev.next, slow = slow, rev, slow.next
        if fast:
            slow = slow.next

        while slow and slow.val == rev.val:
            slow = slow.next
            rev = rev.next
        return not slow
