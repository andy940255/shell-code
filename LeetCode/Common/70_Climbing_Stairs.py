class Solution:
    def climbStairs(self, n: int) -> int:
        pre, cur = 1, 2
        for _ in range(2, n + 1):
            pre, cur = cur, pre + cur
        return pre
