class Solution:
    def minimumLength(self, s: str) -> int:
        s = deque(s)
        while len(s) > 1:
            prefix = s[0]
            suffix = s[-1]
            if prefix != suffix:
                break
            else:
                while s and s[0] == prefix:
                    s.popleft()

                while s and s[-1] == prefix:
                    s.pop()

        return len(s)

    def minimumLength(self, s: str) -> int:
        l, r = 0, len(s) - 1

        while l < r and s[l] == s[r]:
            strr = s[l]
            l += 1
            r -= 1
            while l <= r and s[l] == strr:
                l += 1
            while l <= r and s[r] == strr:
                r -= 1
        return r - l + 1
