class Solution:
    def halvesAreAlike(self, s: str) -> bool:
        n = len(s)
        count = 0
        check = {"a", "e", "i", "o", "u", "A", "E", "I", "O", "U"}
        for i in range(n // 2):
            if s[i] in check:
                count += 1
            if s[n - 1 - i] in check:
                count -= 1
        return count == 0
    
    def halvesAreAlike1(self, s: str) -> bool:
        n = len(s)
        count = 0
        check = {"a", "e", "i", "o", "u", "A", "E", "I", "O", "U"}
        left = s[:n//2]
        right = s[n//2:]
        for c in check:
            count += left.count(c)
            count -= right.count(c)
        return count == 0
