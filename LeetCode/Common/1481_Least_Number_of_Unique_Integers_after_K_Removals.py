from collections import Counter


class Solution:
    def findLeastNumOfUniqueInts(self, arr: List[int], k: int) -> int:
        if k >= len(arr):
            return 0
        if k == 0:
            return len(set(arr))

        arr = Counter(arr).most_common()
        print(arr)
        while k > 0:
            k -= arr.pop()[1]
        if k == 0:
            return len(arr)
        else:
            return len(arr) + 1

    def findLeastNumOfUniqueInts(self, arr: List[int], k: int) -> int:
        if k >= len(arr):
            return 0
        if k == 0:
            return len(set(arr))
        arr = Counter(arr)
        counter, length = Counter(arr.values()), len(arr)
        for key in sorted(counter):
            sub = key * counter[key]
            if k >= sub:
                k -= sub
                length -= counter[key]
            else:
                return length - k // key
        return length
