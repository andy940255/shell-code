nums1 = [1,2,3,4,5]
nums2 = [2,4,8,16]
nums3 = [1,3,5,7,9]




class Solution:
    def hasTrailingZeros(self, nums) -> bool:
        count = 0
        for i in nums:
            if not i % 2:
                count += 1
        return count >= 2
    
s = Solution()
print(s.hasTrailingZeros(nums1))
print(s.hasTrailingZeros(nums2))
print(s.hasTrailingZeros(nums3))