class Solution:
    def pivotInteger(self, n: int) -> int:
        mapp = [i for i in range(1, n + 1)]
        aSum = sum(mapp)
        bSum = 0
        while mapp:
            node = mapp.pop()
            bSum += node
            if aSum == bSum:
                return node
            aSum -= node
        return -1

    def pivotInteger(self, n: int) -> int:
        x = math.sqrt(n * (n + 1) / 2)
        converted = int(x)
        return converted if converted == x else -1
