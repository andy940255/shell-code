class Solution:
    def heightChecker(self, heights: List[int]) -> int:
        ans = 0
        check = sorted(heights)
        for i in range(len(check)):
            if check[i] != heights[i]:
                ans += 1
        return ans
