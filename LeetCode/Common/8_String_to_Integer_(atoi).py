class Solution:
    def myAtoi(self, s: str) -> int:
        s = s.split()
        if not s:
            return 0
        s = s[0]
        isNegative = False 
        num = ""
        first = True
        for i, e in enumerate(s):
            if e == '+':
                if i == 0 and i != len(s)-1 and first:
                    if '0' <= s[i+1] <= '9':
                        continue
                    else:
                        break
                else:
                    break
            elif e == "-":
                if i == 0 and i != len(s)-1 and first:
                    if '0' <= s[i+1] <= '9':
                        isNegative = True
                        continue
                    else:
                        break
                else:
                    break
            elif '0' <= e <= '9':
                num += e
            else:
                break
            if first:
                first = False
        if num == "":
            num = "0"
        num = int(num)
        if isNegative:
            num *= -1
        if num > 2 ** 31 - 1:
            num = 2 ** 31 - 1
        if num < -1 * (2 ** 31):
            num = -1 * (2 ** 31)
        return num