class Solution:
    def maxSumAfterPartitioning(self, arr: List[int], k: int) -> int:
        n = len(arr)
        if k >= n:
            return max(arr) * n

        dp = [0] * (n + 1)
        for i in range(1, n + 1):
            maxNum = float("-inf")
            for j in range(i - 1, max(-1, i - k - 1), -1):
                maxNum = max(maxNum, arr[j])
                dp[i] = max(dp[i], dp[j] + maxNum * (i - j))
        return dp[n]
