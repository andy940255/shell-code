from collections import defaultdict


class Solution:
    def validPath(self, n, edges, source: int, destination: int) -> bool:
        if (
            source == destination
            or [source, destination] in edges
            or [destination, source] in edges
        ):
            return True
        dd = defaultdict(list)
        for i, j in edges:
            dd[i].append(j)
            dd[j].append(i)
        stack = [source]
        visted = set()
        visted.add(source)
        while stack:
            cur = stack.pop()
            if cur == destination:
                return True
            visted.add(cur)
            for i in dd[cur]:
                if i not in visted:
                    stack.append(i)
        return destination in visted
