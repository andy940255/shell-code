class Solution:
    def hammingDistance(self, x: int, y: int) -> int:
        z = x ^ y
        count = 0
        while z > 0:
            z &= z - 1
            count += 1
        return count

    def hammingDistance(self, x: int, y: int) -> int:
        return bin(x ^ y).count("1")
