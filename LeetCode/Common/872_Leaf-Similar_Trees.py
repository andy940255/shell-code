# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right


class Solution:
    def leafSimilar(self, root1, root2) -> bool:
        leaf1 = []
        leaf2 = []

        stack1 = [root1]
        stack2 = [root2]
        while stack1:
            node = stack1.pop()
            if node:
                if not node.left and not node.right:
                    leaf1.append(node.val)
                stack1.append(node.left)
                stack1.append(node.right)

        while stack2:
            node = stack2.pop()
            if node:
                if not node.left and not node.right:
                    leaf2.append(node.val)
                stack2.append(node.left)
                stack2.append(node.right)

        return leaf1 == leaf2

    def leafSimilar1(self, root1, root2) -> bool:
        def dfs(node):
            if not node:
                return
            if not node.left and not node.right:
                yield node.val
            yield from dfs(node.left)
            yield from dfs(node.right)

        return list(dfs(root1)) == list(dfs(root2))
