class Solution:
    def minimumAbsDifference(self, arr: List[int]) -> List[List[int]]:
        arr.sort()
        ans = []
        length = math.inf
        for i in range(1, len(arr)):
            diff = arr[i] - arr[i - 1]
            if diff == length:
                ans.append([arr[i - 1], arr[i]])
            elif diff < length:
                ans = [[arr[i - 1], arr[i]]]
                length = diff
        return ans
