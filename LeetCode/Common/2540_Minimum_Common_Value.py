class Solution:
    def getCommon(self, nums1: List[int], nums2: List[int]) -> int:
        sets1 = set(nums1)
        sets2 = set(nums2)
        if nums1[0] <= nums2[0]:
            if nums1[-1] < nums2[0]:
                return -1
            for num in nums2:
                if num in sets1:
                    return num
        else:
            if nums2[-1] < nums1[0]:
                return -1
            for num in nums1:
                if num in sets2:
                    return num
        return -1

    def getCommon(self, nums1: List[int], nums2: List[int]) -> int:
        index1 = index2 = 0
        length1, length2 = len(nums1), len(nums2)
        while index1 < length1 and index2 < length2:
            if nums1[index1] == nums2[index2]:
                return nums1[index1]
            if nums1[index1] < nums2[index2]:
                index1 += 1
            else:
                index2 += 1
        return -1