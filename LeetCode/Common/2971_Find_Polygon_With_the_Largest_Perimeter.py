class Solution:
    def largestPerimeter(self, nums: List[int]) -> int:
        nums.sort(reverse=True)
        numSum = sum(nums)
        for num in nums:
            numSum -= num
            if numSum > num:
                return numSum + num
        return -1

    def largestPerimeter(self, nums: List[int]) -> int:
        while True:
            if not nums:
                return -1
            maxNum = max(nums)
            nums.remove(maxNum)
            sumNum = sum(nums)
            if sumNum > maxNum:
                return sumNum + maxNum
