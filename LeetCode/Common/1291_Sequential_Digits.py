class Solution:
    def sequentialDigits(self, low: int, high: int) -> List[int]:
        ans = []
        init = "123456789"
        for size in range(2, 10):
            for idx in range(10 - size):
                num = int(init[idx : idx + size])
                if num > high:
                    return ans
                elif num >= low:
                    ans.append(num)
        return ans
