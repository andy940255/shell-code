class Solution:
    def compareVersion(self, version1: str, version2: str) -> int:
        def transfer(w):
            res = []
            words = w.split(".")
            for word in words:
                res.append(int(word))

            return res

        v1 = transfer(version1)
        v2 = transfer(version2)
        l1 = len(v1)
        l2 = len(v2)

        if l1 > l2:
            v2.extend([0] * (l1 - l2))
            maxL = l1
        else:
            v1.extend([0] * (l2 - l1))
            maxL = l2

        for i in range(maxL):
            if v1[i] > v2[i]:
                return 1
            elif v1[i] < v2[i]:
                return -1
        return 0
