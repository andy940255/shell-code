class Solution:
    def dailyTemperatures(self, temperatures: List[int]) -> List[int]:
        stack = []
        n = len(temperatures)
        ans = [0] * n
        for temp in range(n):
            while stack and temperatures[stack[-1]] < temperatures[temp]:
                stackInd = stack.pop()
                ans[stackInd] = temp - stackInd
            stack.append(temp)
        return ans
