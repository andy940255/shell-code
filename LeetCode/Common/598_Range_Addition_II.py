class Solution:
    def maxCount(self, m: int, n: int, ops: List[List[int]]) -> int:
        if not ops:
            return m * n
        arr = [[0] * n for _ in range(m)]
        for op in ops:
            for i in range(op[0]):
                for j in range(op[1]):
                    arr[i][j] += 1
        print(arr, sep="\n")
        counter = collections.Counter()
        for a in arr:
            counter += collections.Counter(a)
        print(counter)
        return counter[max(counter.keys())]

    def maxCount(self, m: int, n: int, ops: List[List[int]]) -> int:
        if ops:
            x, y = zip(*ops)
            return min(x) * min(y)
        return m * n
