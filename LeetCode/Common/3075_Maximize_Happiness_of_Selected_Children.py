class Solution:
    def maximumHappinessSum(self, happiness: List[int], k: int) -> int:
        happiness.sort(reverse=True)
        ans = 0

        while k:
            k -= 1
            check = happiness.pop() - R
            if check <= 0:
                break
            ans += check
            R += 1
        return ans

    def maximumHappinessSum(self, happiness: List[int], k: int) -> int:
        happiness.sort(reverse=True)
        ans = 0
        if happiness[k - 1] >= k - 1:
            return sum(happiness[:k]) - ((k - 1) * k // 2)

        # for i in range(k):
        #     check = happiness[i] - i
        #     if check <= 0:
        #         break
        #     ans += check
        # return ans

        for i in range(k):
            if (check := happiness[i] - i) <= 0:
                break
            ans += check
        return ans
