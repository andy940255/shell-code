# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def removeNodes(self, head: Optional[ListNode]) -> Optional[ListNode]:
        if not head:
            return head
        head.next = self.removeNodes(head.next)
        if head.next and head.val < head.next.val:
            return head.next
        return head

    def removeNodes(self, head: Optional[ListNode]) -> Optional[ListNode]:
        stack = []

        while head:
            node = head
            while stack and stack[-1].val < node.val:
                stack.pop()
            stack.append(node)
            head = head.next

        H = ListNode()
        cur = H
        while stack:
            cur.next = stack.pop(0)
            cur = cur.next

        return H.next
