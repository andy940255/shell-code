class Solution:
    def maxDepth(self, s: str) -> int:
        s = list(s)
        ans = 0
        check = 0
        for i in s:
            check += i == "("
            ans = max(ans, check)
            check -= i == ")"
        return ans
