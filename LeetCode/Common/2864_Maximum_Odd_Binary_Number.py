class Solution:
    def maximumOddBinaryNumber(self, s: str) -> str:
        countOne = s.count("1")
        return "1" * (countOne - 1) + "0" * (len(s) - countOne) + "1"
