from collections import Counter


class Solution:
    def closeStrings(self, word1: str, word2: str) -> bool:
        n1 = len(word1)
        n2 = len(word2)
        if n1 != n2 or set(word1) != set(word2):
            return False

        # return sorted(Counter(word1).values()) == sorted(Counter(word2).values())

        c1 = []
        c2 = []
        for i in set(word1):
            c1.append(word1.count(i))
            c2.append(word2.count(i))
        return sorted(c1) == sorted(c2)
