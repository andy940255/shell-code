class Solution:
    def islandPerimeter(self, grid) -> int:
        ans = 0
        # calculate bound
        xLen = len(grid)
        yLen = len(grid[0])
        for x in range(xLen):
            for y in range(yLen):
                # print(grid[x][y], end=" ")
                if grid[x][y]:
                    ans += 4

                    if x > 0 and grid[x - 1][y]:
                        ans -= 2
                    if y > 0 and grid[x][y - 1]:
                        ans -= 2
            # print()
        return ans


solution = Solution()
print(
    solution.islandPerimeter([[0, 1, 0, 0], [1, 1, 1, 0], [0, 1, 0, 0], [1, 1, 0, 0]])
)
print("-" * 10)
print(solution.islandPerimeter([[1]]))
print("-" * 10)
print(solution.islandPerimeter([[1, 0]]))

"""
0 1 0 0
1 1 1 0
0 1 0 0
1 1 0 0
"""
