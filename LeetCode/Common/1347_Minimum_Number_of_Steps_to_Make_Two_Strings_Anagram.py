from collections import Counter
class Solution:
    def minSteps(self, s: str, t: str) -> int:
        return (Counter(t) - Counter(s)).total()

    def minSteps(self, s: str, t: str) -> int:
        ans = 0
        for char in set(t):
            gap = t.count(char) - s.count(char)
            ans += gap if gap > 0 else 0
        return ans

s = Solution()

# 5
# a = "leetcode"
# b = "practice"
# print(s.minSteps(a, b))


# 18
a = "gctcxyuluxjuxnsvmomavutrrfb"
b = "qijrjrhqqjxjtprybrzpyfyqtzf"
print(s.minSteps(a, b))