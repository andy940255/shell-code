# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right


class Solution:
    def kthSmallest(self, root: Optional[TreeNode], k: int) -> int:
        stack = []
        node = root
        while node or stack:
            while node:
                stack.append(node)
                node = node.left
            node = stack.pop()
            k -= 1
            if k == 0:
                return node.val
            node = node.right
        return None

    def kthSmallest1(self, root: Optional[TreeNode], k: int) -> int:
        def helper(node):
            if not node:
                return 0
            else:
                return 1 + helper(node.left) + helper(node.right)

        counter = helper(root.left)
        if k <= counter:
            return self.kthSmallest(root.left, k)
        elif k > counter + 1:
            return self.kthSmallest(root.right, k - 1 - counter)
        else:
            return root.val
