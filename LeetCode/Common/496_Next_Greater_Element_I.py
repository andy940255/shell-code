class Solution:
    def nextGreaterElement(self, nums1, nums2):
        stack = []
        next_greater = {}

        # 创建nums2中元素的下一个更大元素的映射关系
        for num in nums2:
            print("S: ", num)
            while stack and stack[-1] < num:
                next_greater[stack.pop()] = num
                print(next_greater)
            stack.append(num)
            print("Append: ", num)
        print(next_greater)
        # 根据映射关系找出nums1中每个元素的下一个更大元素
        result = []
        for num in nums1:
            result.append(next_greater.get(num, -1))

        return result


solution = Solution()
print(solution.nextGreaterElement([4, 1, 2], [1, 3, 4, 2]))
print(solution.nextGreaterElement([2, 4], [1, 2, 3, 4]))
print(solution.nextGreaterElement([3], [3, 4]))
