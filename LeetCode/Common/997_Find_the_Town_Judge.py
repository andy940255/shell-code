from collections import defaultdict

class Solution:
    def findJudge(self, n, trust) -> int:
        dic = defaultdict(lambda: 0)
        tPeople = set()
        for t in trust:
            dic[t[1]] += 1
            tPeople.add(t[0])

        for i in range(1, n+1):
            if dic[i] == n-1 and i not in tPeople:
                return i
        return -1



S = Solution()
print(S.findJudge(2, [[1,2]]))
print(S.findJudge(3, [[1,3],[2,3]]))
print(S.findJudge(3, [[1,3],[2,3],[3,1]]))