from collections import defaultdict


class Solution:
    def groupAnagrams(self, strs: List[str]) -> List[List[str]]:
        dic = defaultdict(list)
        for strr in strs:
            dic[tuple(sorted(strr))].append(strr)
        return dic.values()
