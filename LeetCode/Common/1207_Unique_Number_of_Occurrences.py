from collections import Counter 
class Solution:
    def uniqueOccurrences(self, arr) -> bool:
        sett = set()
        counter = Counter(arr)
        for key in counter:
            if counter[key] in sett:
                return False
            else:
                sett.add(counter[key])
        return True 