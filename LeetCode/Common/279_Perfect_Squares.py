class Solution:
    # TLE
    def numSquares(self, n: int) -> int:
        square = []
        for i in range(1, 101):
            if i * i > n:
                break
            square.append(i * i)
        ans = float("inf")
        if n in square:
            return 1
        else:
            for num in square:
                temp = 1 + self.numSquares(n - num)
                ans = min(ans, temp)
        return ans

    # Wrong
    def numSquares(self, n: int) -> int:
        dp = [-1] * (n + 1)
        square = []
        for i in range(1, 101):
            if i * i > n:
                break
            square.append(i * i)
            dp[i * i] = 1

        def helper(num):
            if dp[num] != -1:
                return dp[num]
            else:
                ans = float("inf")
                if num in square:
                    dp[num] = 1
                    return 1
                else:
                    for iNum in square:
                        if num - iNum < 0:
                            break
                        if num - iNum in square:
                            dp[num - iNum] = 1
                            return 2
                        else:
                            temp = 1 + helper(num - iNum)
                            ans = min(ans, temp)
                dp[num] = ans
                return ans

        return helper(n)

    # :D
    def numSquares(self, n: int) -> int:
        dp = [float("inf")] * (n + 1)
        dp[0] = 0

        for i in range(1, n + 1):
            j = 1
            while j * j <= i:
                dp[i] = min(dp[i], dp[i - j * j] + 1)
                j += 1

        return dp[n]
