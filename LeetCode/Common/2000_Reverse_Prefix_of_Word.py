class Solution:
    def reversePrefix(self, word: str, ch: str) -> str:
        ans = []
        reverse = []

        for i, w in enumerate(word):
            if w != ch:
                reverse.append(w)
            else:
                reverse.append(w)
                ans.extend(reverse[::-1])
                ans.extend(word[i + 1 :])
                reverse = []
                return "".join(ans)

        ans.extend(reverse)
        return "".join(ans)

    def reversePrefix(self, word: str, ch: str) -> str:
        ans = []
        for i, w in enumerate(word):
            ans.append(w)
            if w == ch:
                return "".join(ans[: i + 1][::-1] + list(word[i + 1 :]))
        return word
