class Solution:
    def maxProduct(self, nums):
        sorted_nums = sorted(
            [(i, n) for i, n in enumerate(nums)], key=lambda x: x[1], reverse=True
        )
        print(sorted_nums)
        return (sorted_nums[0][1] - 1) * (sorted_nums[1][1] - 1)


s = Solution()
print(s.maxProduct([3, 4, 5, 2]))
