class Solution:
    def findMinArrowShots(self, points: List[List[int]]) -> int:
        newP = sorted(points, key=lambda x: x[1])
        ans = 1
        shot = newP[0][1]

        for i in range(1, len(points)):
            if newP[i][0] > shot:
                ans += 1
                shot = newP[i][1]

        return ans
