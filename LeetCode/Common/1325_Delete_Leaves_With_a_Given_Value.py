# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def removeLeafNodes(
        self, root: Optional[TreeNode], target: int
    ) -> Optional[TreeNode]:
        def remove(node):
            if not node:
                return None
            node.left = remove(node.left)
            node.right = remove(node.right)
            if not node.left and not node.right and node.val == target:
                return None
            return node

        return remove(root)

    def removeLeafNodes(
        self, root: Optional[TreeNode], target: int
    ) -> Optional[TreeNode]:
        if not root:
            return

        if not root.left and not root.right and root.val == target:
            return

        root.left = self.removeLeafNodes(root.left, target)

        root.right = self.removeLeafNodes(root.right, target)

        if not root.left and not root.right and root.val == target:
            return

        return root
