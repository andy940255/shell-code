class Solution:
    def maxArea(self, height) -> int:
        ans = 0
        lenght = len(height) - 1
        left = 0
        right = lenght
        maxH = max(height)
        while left < right and maxH * (right - left) > ans:
            capacity = lenght * min(height[left], height[right])
            ans = max(ans, capacity)
            if height[left] <= height[right]:
                left += 1
            else:
                right -= 1
            lenght -= 1
        return ans


f = open("user.out", "w")
for height in map(loads, stdin):
    ans = 0
    lenght = len(height) - 1
    left = 0
    right = lenght
    maxH = max(height)
    while left < right and maxH * (right - left) > ans:
        capacity = lenght * min(height[left], height[right])
        ans = max(ans, capacity)
        if height[left] <= height[right]:
            left += 1
        else:
            right -= 1
        lenght -= 1
    print(ans, file=f)
exit(0)
