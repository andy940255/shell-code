from collections import Counter


class Solution:
    def findWinners(self, matches):
        winandlose = list(map(list, zip(*matches)))
        members = set(winandlose[0] + winandlose[1])
        counterLost = Counter(winandlose[1])
        ans = [[], []]
        for member in members:
            if counterLost[member] == 0:
                ans[0].append(member)
            elif counterLost[member] == 1:
                ans[1].append(member)
        return ans


s = Solution()


matches = [
    [1, 3],
    [2, 3],
    [3, 6],
    [5, 6],
    [5, 7],
    [4, 5],
    [4, 8],
    [4, 9],
    [10, 4],
    [10, 9],
]
print(s.findWinners(matches))
