class Solution:
    def findRestaurant(self, list1: List[str], list2: List[str]) -> List[str]:
        ans = []
        dic = {}
        commonStrings = list(set(list1) & set(list2))
        for commonString in commonStrings:
            dic[commonString] = list1.index(commonString) + list2.index(commonString)
        minValue = min(dic.values())
        for key, value in dic.items():
            if value == minValue:
                ans.append(key)
        return ans

    def findRestaurant(self, list1: List[str], list2: List[str]) -> List[str]:
        dic = {}
        res = {}
        for i in range(len(list1)):
            dic[list1[i]] = i
        for i in range(len(list2)):
            if list2[i] in dic:
                res[list2[i]] = i + dic[list2[i]]
        minValue = min(res.values())
        ans = []
        for i in res:
            if res[i] == minValue:
                ans.append(i)
        return ans
