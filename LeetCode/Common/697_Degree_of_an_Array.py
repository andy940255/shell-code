from collections import Counter


class Solution:
    def findShortestSubArray(self, nums: List[int]) -> int:
        n = len(nums)
        counter = Counter(nums)
        degree = max(counter.values())
        if degree == 1:
            return 1
        check = [key for key, value in counter.items() if value == degree]

        ans = n
        rNums = nums[::-1]
        for key in check:
            left = operator.indexOf(nums, key)
            right = n - operator.indexOf(rNums, key)
            ans = min(ans, right - left)
        return ans
