class Solution:
    def removeKdigits(self, num: str, k: int) -> str:
        stack = []

        for n in num:
            while k > 0 and stack and n < stack[-1]:
                stack.pop()
                k -= 1

            stack.append(n)

        while k > 0:
            stack.pop()
            k -= 1

        while stack and stack[0] == "0":
            stack.pop(0)

        return "".join(stack) if stack else "0"

    def removeKdigits(self, num: str, k: int) -> str:
        if k >= len(num):
            return "0"

        stack = []

        for digit in num:
            while stack and stack[-1] > digit and k > 0:
                stack.pop()
                k -= 1

            stack.append(digit)

        while k > 0:
            stack.pop()
            k -= 1

        result = "".join(stack).lstrip("0")
        return result if result else "0"
