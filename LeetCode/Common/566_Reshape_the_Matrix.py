from itertools import chain


class Solution:
    def matrixReshape(self, mat: List[List[int]], r: int, c: int) -> List[List[int]]:
        if len(mat) * len(mat[0]) != r * c:
            return mat

        flatten_list = list(chain.from_iterable(mat))

        return [flatten_list[i * c : (i + 1) * c] for i in range(r)]
