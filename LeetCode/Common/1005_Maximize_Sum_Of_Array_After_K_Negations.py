class Solution:
    def largestSumAfterKNegations(self, nums: List[int], k: int) -> int:
        nums.sort()
        for i in range(len(nums)):
            if nums[i] >= 0 or k == 0:
                break
            nums[i] *= -1
            k -= 1

        if k % 2:
            return sum(nums) - 2 * min(nums)
        else:
            return sum(nums)
