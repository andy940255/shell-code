# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


class Solution:
    def mergeKLists(self, lists):
        H = ListNode(-1)
        cur = H
        while lists:
            while None in lists:
                lists.remove(None)
            while [] in lists:
                lists.remove([])
            tempMin = (-1, float("INF"))
            for idx, listt in enumerate(lists):
                if listt and listt.val < tempMin[1]:
                    tempMin = (idx, listt.val)
            if lists:
                cur.next = lists[tempMin[0]]
                if lists[tempMin[0]]:
                    lists[tempMin[0]] = lists[tempMin[0]].next
                cur = cur.next
        return H.next
