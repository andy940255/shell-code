class Solution:
    def isIsomorphic(self, s: str, t: str) -> bool:
        check1 = Counter(s)
        check2 = Counter(t)
        check3 = Counter()
        for i in range(len(s)):
            check3[s[i] + t[i]] += 1
        return (
            sorted(check1.values())
            == sorted(check2.values())
            == sorted(check3.values())
        )

    def isIsomorphic(self, s: str, t: str) -> bool:
        mp1 = {}
        mp2 = {}

        n = len(s)
        for i in range(n):
            if s[i] in mp1 and mp1[s[i]] != t[i]:
                return False
            mp1[s[i]] = t[i]

        for i in range(n):
            if t[i] in mp2 and mp2[t[i]] != s[i]:
                return False
            mp2[t[i]] = s[i]

        return True

    def isIsomorphic(self, s: str, t: str) -> bool:
        return len(set(s)) == len(set(t)) == len(set(zip(s, t)))
