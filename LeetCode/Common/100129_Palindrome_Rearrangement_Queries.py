class Solution:
    def canMakePalindromeQueries(self, s: str, queries: List[List[int]]) -> List[bool]:
        pass


S = Solution()
s = "abcabc"
queries = [[1, 1, 3, 5], [0, 2, 5, 5]]
print(S.canMakePalindromeQueries(s, queries))

s = "abbcdecbba"
queries = [[0, 2, 7, 9]]
print(S.canMakePalindromeQueries(s, queries))

s = "acbcab"
queries = [[1, 2, 4, 5]]
print(S.canMakePalindromeQueries(s, queries))
