class Solution:
    def numRescueBoats(self, people: List[int], limit: int) -> int:
        d = deque(sorted(people, reverse=True))
        count = 0
        while d:
            if d[0] == limit:
                count += 1
                d.popleft()
                continue
            
            capacity = d[0]
            d.popleft()

            while d and capacity + d[-1] <= limit:
                capacity += d.pop()
                break

            count += 1
        return count

    def numRescueBoats(self, people: List[int], limit: int) -> int:
        people.sort()
        l, r = 0, len(people) - 1
        count = 0
        while l <= r:
            if people[l] + people[r] <= limit:
                count += 1
                l += 1
                r -= 1
            else:
                count += 1
                r -= 1
        return count
