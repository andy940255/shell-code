from collections import deque


class Solution:
    def numIslands(self, grid) -> int:
        ans = 0
        X = len(grid)
        Y = len(grid[0])
        visited = [[0] * Y for _ in range(X)]

        def bfs(i, j):
            q = deque([(i, j)])
            while q:
                x, y = q.popleft()
                if (
                    0 <= x < X
                    and 0 <= y < Y
                    and grid[x][y] == "1"
                    and visited[x][y] == 0
                ):
                    visited[x][y] = 1
                    q.extend([(x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)])

        for xx in range(X):
            for yy in range(Y):
                if grid[xx][yy] == "1" and visited[xx][yy] == 0:
                    ans += 1
                    bfs(xx, yy)
        return ans

    def numIslands1(self, grid) -> int:
        res = 0

        def dfs(x, y):
            grid[x][y] = "*"

            if x - 1 >= 0 and grid[x - 1][y] == "1":
                dfs(x - 1, y)
            if x + 1 < len(grid) and grid[x + 1][y] == "1":
                dfs(x + 1, y)
            if y - 1 >= 0 and grid[x][y - 1] == "1":
                dfs(x, y - 1)
            if y + 1 < len(grid[0]) and grid[x][y + 1] == "1":
                dfs(x, y + 1)

        for i in range(len(grid)):
            for j in range(len(grid[0])):
                if grid[i][j] == "1":
                    dfs(i, j)
                    res += 1

        return res
