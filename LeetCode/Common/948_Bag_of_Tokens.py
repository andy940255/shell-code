class Solution:
    def bagOfTokensScore(self, tokens: List[int], power: int) -> int:
        if tokens and min(tokens) > power:
            return 0
        cur = 0
        tokens.sort()
        tokens = collections.deque(tokens)

        while tokens:
            if tokens[0] <= power:
                power -= tokens.popleft()
                cur += 1
            elif len(tokens) > 2:
                power += tokens.pop()
                cur -= 1
            else:
                return cur
        return cur
