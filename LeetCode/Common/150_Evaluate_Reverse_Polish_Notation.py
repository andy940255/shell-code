class Solution:
    def evalRPN(self, tokens: List[str]) -> int:
        stack = []
        opts = ["+", "-", "*", "/"]
        for token in tokens:
            if token in opts:
                b = stack.pop()
                a = stack.pop()
                if token == "+":
                    res = a + b
                elif token == "-":
                    res = a - b
                elif token == "*":
                    res = a * b
                elif token == "/":
                    res = int(a / b)
                stack.append(res)
            else:
                stack.append(int(token))
        return stack.pop()

    def evalRPN(self, tokens: List[str]) -> int:
        stack = []
        opts = {
            "+": lambda x, y: x + y,
            "-": lambda x, y: x - y,
            "*": lambda x, y: x * y,
            "/": lambda x, y: int(x/y)
        }
        for token in tokens:
            if token in opts:
                b = stack.pop()
                a = stack.pop()
                stack.append(opts[token](a, b))
            else:
                stack.append(int(token))
        return stack.pop()