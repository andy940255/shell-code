class Solution:
    def calPoints(self, operations: List[str]) -> int:
        scores = 0
        stack = []
        for op in operations:
            if op == "+":
                stack.append(stack[-1] + stack[-2])
                scores += stack[-1]
            elif op == "D":
                stack.append(stack[-1] * 2)
                scores += stack[-1]
            elif op == "C":
                scores -= stack.pop()
            else:
                stack.append(int(op))
                scores += int(op)
        return scores
