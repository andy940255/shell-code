# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right


class Solution:
    def isSubtree(self, root: Optional[TreeNode], subRoot: Optional[TreeNode]) -> bool:
        def sameTree(nodeA, nodeB):
            if nodeA == None and nodeB == None:
                return True
            if nodeA and nodeB:
                return (
                    nodeA.val == nodeB.val
                    and sameTree(nodeA.left, nodeB.left)
                    and sameTree(nodeA.right, nodeB.right)
                )

        if not subRoot:
            return True
        if not root:
            return False
        if sameTree(root, subRoot):
            return True
        else:
            return self.isSubtree(root.left, subRoot) or self.isSubtree(
                root.right, subRoot
            )
