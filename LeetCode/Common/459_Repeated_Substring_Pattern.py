# class Solution:
#     def repeatedSubstringPattern(self, s: str) -> bool:
#         ls = len(s)
#         if ls <= 1:
#             return False

#         for length in range(1, ls // 2 + 1):
#             if ls % length == 0:
#                 num = ls // length
#                 substr = s[:length]
#                 if substr * num == s:
#                     return True
#         return False


class Solution:
    def repeatedSubstringPattern(self, s: str) -> bool:
        s2 = s + s
        print(s2)
        print(s2[1:-1])
        if s in s2[1:-1]:
            return True
        return False


solution_instance = Solution()
print(solution_instance.repeatedSubstringPattern("abccabcc"))
