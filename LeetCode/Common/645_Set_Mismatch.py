from collections import Counter
from typing import List


class Solution:
    def findErrorNums(self, nums: List[int]) -> List[int]:
        counter = Counter(nums)
        check = [i for i in range(1, len(nums) + 1)]
        missNum = list(check - counter.keys())[0]
        twiceNum = None
        for i in counter:
            if counter[i] == 2:
                twiceNum = i
        return [twiceNum, missNum]
