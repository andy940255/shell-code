class Solution:
    def tribonacci(self, n: int) -> int:
        t0 = 0
        t1 = 1
        t2 = 1

        if n == 0:
            return t0
        elif n == 1:
            return t1
        elif n == 2:
            return t2

        while n > 2:
            n -= 1
            t0, t1, t2 = t1, t2, t0 + t1 + t2
        return t2

    def tribonacci(self, n: int) -> int:
        ans = [0, 1, 1]
        if n < 3:
            return ans[n]
        else:
            for i in range(3, n + 1):
                # ans[0], ans[1], ans[2] = ans[1], ans[2], ans[0]+ ans[1]+ ans[2]
                ans = [ans[1], ans[2], ans[0] + ans[1] + ans[2]]
            return ans[2]
