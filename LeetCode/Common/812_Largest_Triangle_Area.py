class Solution:

    def largestTriangleArea(self, points: List[List[int]]) -> float:
        def getDist(x1, y1, x2, y2):
            return math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)

        def getArea(x1, y1, x2, y2, x3, y3):
            s1 = getDist(x1, y1, x2, y2)
            s2 = getDist(x2, y2, x3, y3)
            s3 = getDist(x3, y3, x1, y1)
            p = (s1 + s2 + s3) / 2
            if s1 + s2 > s3 and s2 + s3 > s1 and s3 + s1 > s2:
                return round(math.sqrt(p * (p - s1) * (p - s2) * (p - s3)), 4)
            else:
                return 0

        ans = 0.00
        n = len(points)
        for a in range(n - 2):
            for b in range(a, n - 1):
                for c in range(b, n):
                    ans = max(
                        ans,
                        getArea(
                            points[a][0],
                            points[a][1],
                            points[b][0],
                            points[b][1],
                            points[c][0],
                            points[c][1],
                        ),
                    )
        return ans
