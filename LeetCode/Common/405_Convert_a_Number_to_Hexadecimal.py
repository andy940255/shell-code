# class Solution:
#     def toHex(self, num: int) -> str:
#         transArr = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f"]
#         ans = ""
#         if num < 0:
#             negative = True
#             num *= -1
#         else:
#             negative = False
#         while num > 0:
#             r = num % 16
#             num //= 16
#             ans = transArr[r] + ans
#         print(ans)
#         if negative:
#             ans = (8 - len(ans)) * "0" + ans
#             print(ans)
#             reverseAns = ""
#             for bit in ans:
#                 reverseAns += transArr[15-transArr.index(bit)]
#             print(reverseAns)

#             add = 0
#             addd = 1
#             for index in range(7, -1, -1):
#                 result = transArr.index(reverseAns[index]) + addd + add
#                 addd = 0
#                 if result > 15:
#                     add = 1
#                 else:
#                     add = 0
#                 reverseAns = reverseAns[:index] + transArr[result % 16] + reverseAns[index+1:]
#             print(reverseAns)
#             ans = reverseAns

#         return ans


class Solution:
    def toHex(self, num: int) -> str:
        if not num:
            return "0"
        hexmap = "0123456789abcdef"
        ans = []
        for _ in range(8):
            slide = hexmap[num & 15]
            ans.append(slide)
            num >>= 4
            if not num:
                break
        return "".join(ans[::-1])


solution_instance = Solution()
print(solution_instance.toHex(-1563))
