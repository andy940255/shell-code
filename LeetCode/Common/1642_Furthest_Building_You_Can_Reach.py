from typing import List
import heapq


class Solution:
    #     def furthestBuilding(self, heights: List[int], bricks: int, ladders: int) -> int:
    #         n = len(heights)
    #         jump = [0] * n
    #         for i in range(n-1):
    #             jump[i] = max(0, heights[i+1] - heights[i])
    #         cur = 0
    #         useB = []
    #         useL = []
    #         while cur < n-1:
    #             if jump[cur] == 0:
    #                 cur += 1
    #             elif bricks >= jump[cur]:
    #                 bricks -= jump[cur]
    #                 useB.append(jump[cur])
    #                 cur += 1
    #             else:
    #                 if ladders > 0:
    #                     useB.append(jump[cur])
    #                     bMax = max(useB)
    #                     bricks += bMax - jump[cur]
    #                     useB.remove(bMax)
    #                     ladders -= 1
    #                     useL.append(bMax)
    #                     cur += 1
    #                 else:
    #                     B = max(useB, default = float("-inf"))
    #                     L = min(useL, default = float("inf"))
    #                     if B > L:
    #                         useB.remove(B)
    #                         useL.remove(L)
    #                         useB.append(L)
    #                         useL.append(B)
    #                         bricks += B

    #                     return cur
    #         return cur

    def furthestBuilding(self, heights: List[int], bricks: int, ladders: int) -> int:
        minHeap = []
        usedBricks = 0
        n = len(heights)

        for i in range(n - 1):
            diff = heights[i + 1] - heights[i]
            if diff > 0:
                heapq.heappush(minHeap, diff)
                if len(minHeap) > ladders:
                    usedBricks += heapq.heappop(minHeap)
                    if usedBricks > bricks:
                        return i

        return n - 1
