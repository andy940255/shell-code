# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right


class Solution:
    def maxAncestorDiff(self, root) -> int:
        self.ans = 0

        def f(node, maxNum, minNum):
            if node:
                if node.val > maxNum:
                    maxNum = node.val
                if node.val < minNum:
                    minNum = node.val
                self.ans = max(self.ans, maxNum - minNum)
                f(node.left, maxNum, minNum)
                f(node.right, maxNum, minNum)

        f(root, root.val, root.val)
        return self.ans

    def maxAncestorDiff1(self, root) -> int:
        self.ans = 0

        def f(node, maxNum, minNum):
            if node:
                f(node.left, max(maxNum, node.val), min(minNum, node.val))
                f(node.right, max(maxNum, node.val), min(minNum, node.val))
            else:
                self.ans = max(self.ans, maxNum - minNum)

        f(root, root.val, root.val)
        return self.ans
