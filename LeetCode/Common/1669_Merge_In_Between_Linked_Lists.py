# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, val=0, next=None):
#         self.val = val
#         self.next = next
class Solution:
    def mergeInBetween(
        self, list1: ListNode, a: int, b: int, list2: ListNode
    ) -> ListNode:
        findHead = list1
        for i in range(a - 1):
            findHead = findHead.next
        findTail = findHead.next
        findHead.next = list2
        while list2.next:
            list2 = list2.next
        for i in range(b - a + 1):
            findTail = findTail.next
        list2.next = findTail
        return list1
