from collections import defaultdict


class Solution:
    def findCenter(self, edges) -> int:
        dd = defaultdict(int)

        for i, j in edges:
            dd[i] += 1
            dd[j] += 1

        return max(dd, key=dd.get)

    def findCenter1(self, edges) -> int:
        return (
            edges[0][0]
            if edges[0][0] == edges[1][0] or edges[0][0] == edges[1][1]
            else edges[0][1]
        )
