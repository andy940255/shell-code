class Solution:
    def minFallingPathSum(self, dp: List[List[int]]) -> int:
        n = len(dp)
        for row in range(1, n):
            for col in range(n):
                dp[row][col] += min(dp[row - 1][max(0, col - 1) : min(n, col + 2)])
        return min(dp[-1])
