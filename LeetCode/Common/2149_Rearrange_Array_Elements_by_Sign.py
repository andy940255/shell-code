from collections import deque


class Solution:
    def rearrangeArray(self, nums: List[int]) -> List[int]:
        positive = deque()
        negative = deque()
        ans = []
        flag = True
        for num in nums:
            if num > 0:
                if flag:
                    ans.append(num)
                    flag = False
                else:
                    if negative:
                        ans.append(negative.popleft())
                        ans.append(num)
                        flag = False

                    else:
                        positive.append(num)
            else:
                if not flag:
                    ans.append(num)
                    flag = True
                else:
                    if positive:
                        ans.append(positive.popleft())
                        ans.append(num)
                        flag = True
                    else:
                        negative.append(num)

            while flag and positive or not flag and negative:
                if flag and positive:
                    ans.append(positive.popleft())
                    flag = False
                elif not flag and negative:
                    ans.append(negative.popleft())
                    flag = True
        return ans

    def rearrangeArray(self, nums: List[int]) -> List[int]:
        positive = 0
        negative = 1
        cur = 0
        ans = [0] * len(nums)

        for num in nums:
            if num > 0:
                cur = positive
                positive += 2
            else:
                cur = negative
                negative += 2
            ans[cur] = num
        return ans
