class Solution:
    def judgeCircle(self, moves: str) -> bool:
        verticalCounter = 0
        horizontalCounter = 0
        for move in moves:
            if move == "U":
                verticalCounter += 1
            if move == "D":
                verticalCounter -= 1
            if move == "R":
                horizontalCounter += 1
            if move == "L":
                horizontalCounter -= 1
        return verticalCounter == 0 and horizontalCounter == 0

    def judgeCircle(self, moves: str) -> bool:
        return moves.count("U") == moves.count("D") and moves.count("R") == moves.count(
            "L"
        )
