class Solution:
    def numIdenticalPairs(self, nums: List[int]) -> int:
        ans = 0
        dic = {}
        for num in nums:
            if num not in dic:
                dic[num] = 1
            else:
                ans += dic[num]
                dic[num] += 1
        return ans
