class Solution:
    def countStudents(self, students: List[int], sandwiches: List[int]) -> int:
        students = deque(students)
        sandwiches = deque(sandwiches)
        count = 0
        while count < len(students) and students:

            curStudent = students.popleft()
            if curStudent == sandwiches[0]:
                count = 0
                sandwiches.popleft()
            else:
                count += 1
                students.append(curStudent)

        return len(students)

    def countStudents(self, students: List[int], sandwiches: List[int]) -> int:
        students = deque(students)
        sandwiches = deque(sandwiches)
        while students:
            temp = students.copy()
            cur = students.popleft()
            if cur == sandwiches[0]:
                sandwiches.popleft()
            else:
                students.append(cur)
                if students == temp:
                    return len(students)
        return 0
