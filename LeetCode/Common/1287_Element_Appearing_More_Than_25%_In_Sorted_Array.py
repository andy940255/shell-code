class Solution:
    def findSpecialInteger(self, arr) -> int:
        def binary_search(target, is_first):
            left, right = 0, len(arr) - 1
            result = -1

            while left <= right:
                mid = (left + right) // 2
                print(left, mid, right)

                if arr[mid] == target:
                    result = mid
                    if is_first:
                        right = mid - 1
                    else:
                        left = mid + 1
                elif arr[mid] < target:
                    left = mid + 1
                else:
                    right = mid - 1

            return result

        n = len(arr)
        quarter = n // 4
        print("quarter: ", quarter)

        # Handle the case where quarter is zero
        if quarter == 0:
            return arr[0] if n > 0 else None

        # Check every possible candidate element
        for i in range(quarter, n, quarter):
            print(i, " -----------------------")
            # Use binary search to find the first and last occurrence of the candidate element
            left_occurrence = binary_search(arr[i], True)
            print("left:\t", left_occurrence)
            right_occurrence = binary_search(arr[i], False)
            print("Right:\t", right_occurrence)

            # Check if the frequency is greater than 25%
            if right_occurrence - left_occurrence + 1 > quarter:
                return arr[i]


solution = Solution()
print(
    solution.findSpecialInteger(
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 6, 6, 6, 6, 7, 10]
    )
)
