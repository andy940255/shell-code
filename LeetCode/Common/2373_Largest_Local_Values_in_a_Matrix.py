class Solution:
    def largestLocal(self, grid: List[List[int]]) -> List[List[int]]:
        def cal(x, y):
            maxValue = 0
            for xx in range(3):
                for yy in range(3):
                    maxValue = max(maxValue, grid[x + xx][y + yy])
            return maxValue

        ans = []
        n = len(grid)
        for i in range(n - 2):
            temp = []
            for j in range(n - 2):
                temp.append(
                    max(
                        grid[i + 0][j + 0],
                        grid[i + 0][j + 1],
                        grid[i + 0][j + 2],
                        grid[i + 1][j + 0],
                        grid[i + 1][j + 1],
                        grid[i + 1][j + 2],
                        grid[i + 2][j + 0],
                        grid[i + 2][j + 1],
                        grid[i + 2][j + 2],
                    )
                )
            ans.append(temp)

        return ans
