class Solution:
    def findMaxK(self, nums: List[int]) -> int:
        ans = -1
        nums = set(nums)
        for num in nums:
            if num > 0 and num > ans and num * -1 in nums:
                ans = num
        return ans