class Solution:
    def makeGood(self, s: str) -> str:
        ans = deque()
        for i in s:
            if (
                ans and abs(ord(i) - ord(ans[-1])) == 32
            ):  # 32 = abs(ord('A') - ord('a'))
                ans.pop()
            else:
                ans.append(i)
        return "".join(ans)

    def makeGood(self, s: str) -> str:
        # ans = deque()
        ans = []
        for i in s:
            if ans and i.swapcase() == ans[-1]:
                ans.pop()
            else:
                ans.append(i)
        return "".join(ans)
