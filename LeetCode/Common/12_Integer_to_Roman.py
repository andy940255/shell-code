class Solution:
    def intToRoman(self, num: int) -> str:
        T = ["", "M", "MM", "MMM"]
        H = ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"]
        N = ["", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"]
        O = ["", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"]
        return (
            T[num // 1000] + H[(num % 1000) // 100] + N[(num % 100) // 10] + O[num % 10]
        )

import time
a = time.time()
print(a)