from collections import Counter


class Solution:
    def maximumLength(self, s: str) -> int:
        if len(set(s)) == 1:
            return len(s) - 2
        print()
        ans = -1
        counter = Counter(s)
        for c, count in counter.items():
            if count > max(ans, 2):
                for i in range(count, 0, -1):  # 从出现次数开始向下迭代
                    substr = c * i
                    nums = 0
                    idx = 0
                    while True:
                        idx = s.find(substr, idx)
                        if idx == -1:
                            break
                        nums += 1
                        if nums >= 3:
                            break
                        idx += 1
                    if nums >= 3:  # 至少出现三次
                        ans = max(ans, len(substr))
                        break
        return ans


S = Solution()
print(S.maximumLength("a" * 2500 + "b" * 2500))  # 2498
print(S.maximumLength("a" * 4))  # 2
print(S.maximumLength("abcdef"))  # -1
print(S.maximumLength("abcaba"))  # 1
print(S.maximumLength("aaaajkbdfaaaa"))  # 3
print(S.maximumLength("abcccccdddd"))  # 3
print(S.maximumLength("aaaj"))  # 1
