# The guess API is already defined for you.
# @param num, your guess
# @return -1 if num is higher than the picked number
#          1 if num is lower than the picked number
#          otherwise return 0
# def guess(num: int) -> int:

class Solution:
    def guessNumber(self, n: int) -> int:
        left, right = 1, n
        while left <= right:
            mid = (left + right) // 2
            g = guess(mid)
            if g == 1:
                left = mid + 1
            elif g == -1:
                right = mid - 1
            else:
                return mid
        
    def guessNumber(self, n: int) -> int:
        curr = n // 2
        factor = n // 4
        while True:
            guessed = guess(curr)
            if guessed == 0:
                return curr
            elif guessed == -1:
                curr -= factor
            else:
                curr += factor
            factor //= 2
            if factor == 0:
                factor = 1