text1 = "aabbccdd"
text2 = "abbfddcd"
# abbcd
# V1
# l1 = len(text1) # 5
# l2 = len(text2) # 3

# dp = [[0] * (l2+1) for _ in range(l1+1)]
# lcs = ""
# print(dp)

# for i in range(1, l1+1):
#     for j in range(1, l2+1):
#         print(i, j, end="\t")
#         if text1[i-1] == text2[j-1]:
#             dp[i][j] = dp[i-1][j-1] + 1
#             lcs += text1[i-1]
#         else:
#             dp[i][j] = max(dp[i-1][j], dp[i][j-1])
#     print()
# print(lcs)

# V2


def longestCommonSubsequence(text1: str, text2: str) -> str:
    if len(text1) < len(text2):
        text1, text2 = text2, text1
    l1 = len(text1)
    l2 = len(text2)

    dp = [0] * (l2 + 1)
    lcs = [""] * (l2 + 1)

    for i in range(1, l1 + 1):
        prev = 0
        prestr = ""
        for j in range(1, l2 + 1):
            cur = dp[j]
            curstr = lcs[j]
            if text1[i - 1] == text2[j - 1]:
                dp[j] = prev + 1
                lcs[j] = prestr + text1[i - 1]
            else:
                if dp[j - 1] >= dp[j]:
                    lcs[j] = lcs[j - 1]
                else:
                    lcs[j] = lcs[j]
                dp[j] = max(dp[j], dp[j - 1])
            prev = cur
            prestr = curstr
    print(lcs)
    print(dp)
    return dp[l2]


print(longestCommonSubsequence(text1, text2))
