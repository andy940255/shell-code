# Definition for a binary tree node.
class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def findBottomLeftValue(self, root: Optional[TreeNode]) -> int:

        def deep(node, num):
            if not node:
                return (None, num)

            left = deep(node.left, num)
            right = deep(node.right, num)
            # print(f"node:{node.val}, num:{num}, left:{left}, right:{right}")
            if node.left and node.right:
                if left[1] >= right[1]:
                    return (left[0], left[1] + 1)
                else:
                    return (right[0], right[1] + 1)
            elif node.left:
                return (left[0], left[1] + 1)
            elif node.right:
                return (right[0], right[1] + 1)
            else:
                return (node, num)

        return deep(root, 0)[0].val
