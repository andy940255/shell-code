import itertools
from collections import defaultdict

# class Solution:
#     def maximumLength(self, s: str) -> int:
#         myStore = defaultdict(list)
#         for k, g in itertools.groupby(s):
#             print(k)
#             print(list(g))
#             print("-"*10)
#             myStore[k].append(len(list(g)))

#         ans = -1
#         for key in myStore:
#             v = sorted(myStore[key], reverse=True)
#             # print(key, v)
#             if sum(v) < 3:
#                 continue
#             if len(v) >= 1:
#                 ans = max(ans, v[0] - 2)
#             if len(v) >= 2:
#                 ans = max(ans, min(v[0] - 1, v[1]))
#             if len(v) >= 3:
#                 ans = max(ans, v[2])
#         return ans


class Solution:
    def maximumLength(self, s: str) -> int:
        n = len(s)
        map = defaultdict(list)
        l = 0
        while l < n:
            r = l
            while r < n and s[l] == s[r]:
                r += 1
            for i in range(r - l, max(0, r - l - 3), -1):
                map[s[l]].append(i)
            l = r
        res = -1
        for key in map:
            if len(map[key]) >= 3:
                res = max(res, sorted(map[key])[-3])
        return res


S = Solution()
print(S.maximumLength("a"))
print(S.maximumLength("aa"))
print(S.maximumLength("aaa"))
print(S.maximumLength("aaaa"))
print(S.maximumLength("aaaaa"))
print(S.maximumLength("aaaaaaaaaa"))
print(S.maximumLength("aaaaabaaaaa"))
print(S.maximumLength("abcdef"))
# print(S.maximumLength("abcaba"))
