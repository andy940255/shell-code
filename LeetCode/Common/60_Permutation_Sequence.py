import math


class Solution:
    def getPermutation(self, n: int, k: int) -> str:
        def fact(n):
            if n == 0 or n == 1:
                return 1
            else:
                return n * fact(n - 1)

        factList = [fact(x) for x in range(0, 10)]
        print(factList)

        def getValFromArr(l, k):
            n = len(l)
            mod = factList[n - 1]
            pos = l[math.ceil(k / mod) - 1]
            new_k = k % mod
            return pos, new_k

        inputlist = [x for x in range(1, n + 1)]
        res = []
        while len(inputlist) > 0:
            pos, k = getValFromArr(inputlist, k)
            inputlist.remove(pos)
            res.append(str(pos))
        return "".join(res)


S = Solution()
print(S.getPermutation(10, 3))
