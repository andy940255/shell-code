from itertools import combinations


class CombinationIterator:
    def __init__(self, characters: str, combinationLength: int):
        self.cur = 0
        self.listt = list(combinations(characters, combinationLength))
        self.length = len(self.listt)

    def next(self) -> str:
        result = self.listt[self.cur]
        self.cur += 1
        return "".join(result)

    def hasNext(self) -> bool:
        return self.cur != self.length


# Your CombinationIterator object will be instantiated and called as such:
# obj = CombinationIterator(characters, combinationLength)
# param_1 = obj.next()
# param_2 = obj.hasNext()
